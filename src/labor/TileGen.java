package labor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

import de.hetzge.sgame.misc.E_Orientation;

public class TileGen {

	public static void main(String[] args) {

	}

	public static interface Sized {
		int getWidth();

		int getHeight();
	}

	public static interface Source extends Sized {
		Color get(int x, int y);
	}

	public static interface Target extends Source {
		void set(int x, int y, Color color);
	}

	public static interface Effect {
		void apply(Target target);
	}

	public static class FillEffect implements Effect {
		protected final Source source;

		public FillEffect(Source source) {
			this.source = source;
		}

		@Override
		public void apply(Target target) {
			for (int y = 0; y < target.getHeight(); y++) {
				for (int x = 0; x < target.getWidth(); x++) {
					target.set(x, y, this.source.get(x, y));
				}
			}
		}
	}

	public static class BlankTarget implements Target {
		private final Pixmap pixmap;

		public BlankTarget(int width, int height) {
			this.pixmap = new Pixmap(width, height, Format.RGBA8888);
		}

		@Override
		public Color get(int x, int y) {
			return new Color(this.pixmap.getPixel(x, y));
		}

		@Override
		public int getWidth() {
			return this.pixmap.getWidth();
		}

		@Override
		public int getHeight() {
			return this.pixmap.getHeight();
		}

		@Override
		public void set(int x, int y, Color color) {
			this.pixmap.drawPixel(x, y, Color.rgba8888(color));
		}
	}

	public static abstract class SizelessSource implements Source {
		@Override
		public int getWidth() {
			return Integer.MAX_VALUE;
		}

		@Override
		public int getHeight() {
			return Integer.MAX_VALUE;
		}
	}

	public static abstract class AdvancedSource implements Source {
		protected final Source source;

		public AdvancedSource(Source source) {
			this.source = source;
		}

		@Override
		public int getWidth() {
			return this.source.getWidth();
		}

		@Override
		public int getHeight() {
			return this.source.getHeight();
		}
	}

	public static abstract class AdvancedTarget implements Target {
		protected final Target target;

		public AdvancedTarget(Target target) {
			this.target = target;
		}

		@Override
		public int getWidth() {
			return this.target.getWidth();
		}

		@Override
		public int getHeight() {
			return this.target.getHeight();
		}
	}

//	public static class CenteredTarget extends AdvancedTarget {
//		private final int centeredWidth;
//		private final int centeredHeight;
//
//		public CenteredTarget(Target target, int centeredWidth, int centeredHeight) {
//			super(target);
//			if (centeredWidth > target.getWidth() || centeredHeight > target.getHeight()) {
//				throw new IllegalArgumentException("centered size have to be smaller then orginal target");
//			}
//			this.centeredWidth = centeredWidth;
//			this.centeredHeight = centeredHeight;
//		}
//
//		@Override
//		public Color get(int x, int y) {
//			return null;
//		}
//
//		@Override
//		public int getWidth() {
//			// TODO Auto-generated method stub
//			return 0;
//		}
//
//		@Override
//		public int getHeight() {
//			return 0;
//		}
//
//		@Override
//		public void set(int x, int y, Color color) {
//			// TODO Auto-generated method stub
//
//		}
//		
//		private int xOffset(){
//			
//		}
//		
//		private int yOffset(){
//			
//		}
//		
//		private int offset(){
//			
//		}
//
//	}

	public static class RepeatingSource extends SizelessSource {
		private final Source source;

		public RepeatingSource(Source source) {
			this.source = source;
		}

		@Override
		public Color get(int x, int y) {
			return this.source.get(x - x % this.source.getWidth(), y - y % this.source.getHeight());
		}
	}

	public static class ColorSource extends SizelessSource {
		private final Color color;

		public ColorSource(Color color) {
			this.color = color;
		}

		@Override
		public Color get(int x, int y) {
			return this.color;
		}
	}

	public static class SizedSource extends AdvancedSource {
		public SizedSource(Source source) {
			super(source);
		}

		@Override
		public Color get(int x, int y) {
			return x >= getWidth() || y >= getHeight() ? new Color(0f, 0f, 0f, 0f) : this.source.get(x, y);
		}
	}

	public static class RotatedSource extends AdvancedSource {
		private final E_Orientation orientation;

		public RotatedSource(Source source, E_Orientation orientation) {
			super(source);
			this.orientation = orientation;
		}

		@Override
		public Color get(int x, int y) {
			return null;
		}

		@Override
		public int getWidth() {
			return isSizeFlipped() ? this.source.getHeight() : super.getHeight();
		}

		@Override
		public int getHeight() {
			return isSizeFlipped() ? this.source.getWidth() : super.getHeight();
		}

		private boolean isSizeFlipped() {
			return this.orientation == E_Orientation.EAST || this.orientation == E_Orientation.WEST;
		}
	}

	public static class PixelSource implements Source {
		private final Pixmap pixmap;

		public PixelSource(Pixmap pixmap) {
			this.pixmap = pixmap;
		}

		@Override
		public Color get(int x, int y) {
			return new Color(this.pixmap.getPixel(x, y));
		}

		@Override
		public int getWidth() {
			return this.pixmap.getWidth();
		}

		@Override
		public int getHeight() {
			return this.pixmap.getHeight();
		}
	}

}
