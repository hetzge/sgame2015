package labor;

import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import de.hetzge.sgame.App;
import de.hetzge.sgame.misc.FogOfWar;

public class Blend implements ApplicationListener {

	private static final int NO_MASK = 0xFF;

	public static void main(String[] args) {
		LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
		configuration.title = "Game";
		configuration.width = App.settings.getSystemSettings().getResolutionX();
		configuration.height = App.settings.getSystemSettings().getResolutionY();
		ShaderProgram.pedantic = false;

		new LwjglApplication(new Blend(), configuration);
	}

	SpriteBatch batch;
	OrthographicCamera camera;
	Texture a;
	Texture b;
	Texture c;
	TextureRegion cRegion;
	ShapeRenderer shapes;
	FrameBuffer backBuffer;
	TextureRegion backTexture;
	FrameBuffer lightBuffer;
	TextureRegion lightTexture;
	FogOfWar fogOfWar;

	@Override
	public void create() {
		this.batch = new SpriteBatch();
		this.camera = new OrthographicCamera();
		this.a = new Texture(Gdx.files.external("a.png"));
		this.b = new Texture(Gdx.files.external("b.png"));
		this.c = new Texture(Gdx.files.external("c.png"));
		this.c.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		this.cRegion = new TextureRegion(this.c, 2000, 2000);
		this.cRegion.flip(false, true);
		this.shapes = new ShapeRenderer();
		this.backBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		this.backTexture = new TextureRegion(this.backBuffer.getColorBufferTexture());
		this.lightBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		this.lightTexture = new TextureRegion(this.lightBuffer.getColorBufferTexture());
		this.lightTexture.flip(false, true);
		this.fogOfWar = new FogOfWar();
	}

	@Override
	public void resize(int width, int height) {
		this.camera.setToOrtho(false, width, height);
		this.batch.setProjectionMatrix(this.camera.combined);
	}

	void drawMaskShape() {

	}

	@Override
	public void render() {

		final float size = 100;

		final Texture one = this.a;
		final Texture two = this.c;

		final List<Integer> blendFunctions = Arrays.asList(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_DST_ALPHA);
		// final List<Integer> blendFunctions = Arrays.asList(GL20.GL_ZERO,
		// GL20.GL_ONE, GL20.GL_ALPHA, GL20.GL_SRC_ALPHA, GL20.GL_DST_ALPHA,
		// GL20.GL_ONE_MINUS_SRC_COLOR, GL20.GL_ONE_MINUS_SRC_ALPHA,
		// GL20.GL_ONE_MINUS_DST_COLOR, GL20.GL_ONE_MINUS_DST_ALPHA);
		final List<Integer> equations = Arrays.asList(GL20.GL_FUNC_ADD, GL20.GL_FUNC_SUBTRACT, GL20.GL_FUNC_REVERSE_SUBTRACT);

		Gdx.gl.glClearColor(0f, 1f, 0f, 0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		
		
		this.batch.begin();
		this.batch.draw(this.b, 0, 0);
		this.batch.end();

		
		this.lightBuffer.begin();
		
		Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		this.batch.enableBlending();

		for (int x = 0; x < blendFunctions.size(); x++) {
			for (int y = 0; y < blendFunctions.size(); y++) {
				this.batch.setBlendFunction(GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_SRC_ALPHA);
				// this.batch.setBlendFunction(blendFunctions.get(x),
				// blendFunctions.get(y));
				this.batch.begin();
				this.batch.draw(one, x * size, y * size, size, size);
				this.batch.end();
			}
		}

		for (int x = 0; x < blendFunctions.size(); x++) {
			for (int y = 0; y < blendFunctions.size(); y++) {
				this.batch.setBlendFunction(GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_SRC_ALPHA);
				// this.batch.setBlendFunction(blendFunctions.get(x),
				// blendFunctions.get(y));
				this.batch.begin();
				this.batch.draw(two, x * size, y * size, size, size);
				this.batch.end();
			}
		}

		this.batch.disableBlending();
		this.lightBuffer.end();

//		this.batch.begin();
//		this.batch.enableBlending();
//		this.batch.setBlendFunction(GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_DST_ALPHA);
//		this.batch.draw(this.lightTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//		this.batch.disableBlending();
//		this.batch.end();

		// ############

		// this.fogOfWar.draw(() -> {
		// this.batch.begin();
		//
		// this.batch.setProjectionMatrix(this.camera.combined);
		// this.batch.setColor(1f, 1f, 1f, 1f);
		//
		// this.batch.draw(this.a, 10f, 10f);
		//
		// this.batch.end();
		// });
		//
		// this.batch.begin();
		// this.batch.draw(this.b, 0f, 0f);
		// this.batch.end();
		//
		// this.fogOfWar.drawTo(this.batch);

		// ############

		// this.lightBuffer.begin();
		//
		// Gdx.gl.glBlendFunc(GL20.GL_ONE, GL20.GL_SRC_COLOR);
		// Gdx.gl.glEnable(GL20.GL_BLEND);
		//
		// // clear lightbuffer
		// Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		// Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//
		// Gdx.gl.glBlendFunc(GL20.GL_ZERO, GL20.GL_SRC_ALPHA);
		// Gdx.gl.glEnable(GL20.GL_BLEND);
		//
		// // start rendering the lights
		// this.batch.setProjectionMatrix(this.camera.combined);
		// this.batch.begin();
		// // set the color of your light (red,green,blue,alpha values)
		// this.batch.setColor(1f, 1f, 1f, 1f);
		//
		// this.batch.draw(this.a, 10f, 10f);
		// this.batch.draw(this.a, 100f, 100f);
		//
		// this.batch.end();
		// this.lightBuffer.end();
		//
		// Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		// Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//
		// this.batch.begin();
		// this.batch.draw(this.b, 0f, 0f);
		// this.batch.end();
		//
		// this.batch.setProjectionMatrix(this.camera.combined);
		// this.batch.enableBlending();
		// this.batch.setBlendFunction(GL20.GL_ZERO, GL20.GL_SRC_COLOR);
		//
		// this.batch.begin();
		// this.batch.draw(this.lightTexture, 0, 0, Gdx.graphics.getWidth(),
		// Gdx.graphics.getHeight());
		// this.batch.end();
		//
		// this.batch.setBlendFunction(GL20.GL_SRC_ALPHA,
		// GL20.GL_ONE_MINUS_SRC_ALPHA);

		// http://stackoverflow.com/questions/36079278/libgdx-framebuffer-for-fog-of-war-effect

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		this.batch.dispose();
		this.a.dispose();
		this.shapes.dispose();
	}
}