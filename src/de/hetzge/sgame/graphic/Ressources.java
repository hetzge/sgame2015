package de.hetzge.sgame.graphic;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.IntMap;

import de.hetzge.sgame.App;
import de.hetzge.sgame.item.E_Item;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.render.shader.ReplaceColorShader;
import de.hetzge.sgame.world.TileSet;

public class Ressources {

	private final BasicTileSet basicTileSet = new BasicTileSet();
	private final List<TileSet> tileSets = Arrays.asList(this.basicTileSet);

	private Skin skin;
	private List<TextureRegion> tiles;
	private IntMap<Animation> graphics;
	private TextureRegion[] items;
	private BitmapFont bitmapFont;

	private Pixmap cursor;
	private Texture border;
	private Texture fogOfWar;
	private ShaderProgram replaceColorShaderProgram;
	private ReplaceColorShader replaceColorShader;

	public void init() {
		this.skin = new Skin(Gdx.files.internal("asset/skin/uiskin.json"));
		this.bitmapFont = this.skin.getFont(Constant.DEFAULT_FONT_NAME);
		this.tiles = App.ressourceFunction.loadTiles(this.tileSets);
		this.graphics = App.ressourceFunction.loadGraphics();
		this.items = App.ressourceFunction.loadItems();

		TextureData cursorTextureData = new Texture(Gdx.files.internal("asset/cursor.png")).getTextureData();
		cursorTextureData.prepare();
		this.cursor = cursorTextureData.consumePixmap();

		this.border = new Texture(Gdx.files.internal("asset/border.png"));
		this.fogOfWar = createFogOfWar(Constant.FOG_OF_WAR_RADIUS, 0);

		String fragmentShaderString = Gdx.files.internal("asset/shader/replace_color_fragment.glsl").readString();
		String vertexShaderString = Gdx.files.internal("asset/shader/replace_color_vertex.glsl").readString();
		this.replaceColorShaderProgram = new ShaderProgram(vertexShaderString, fragmentShaderString);
		this.replaceColorShader = new ReplaceColorShader(App.libGdxApplication.getSpriteBatch());
	}

	public void registerGraphic(IF_GraphicKey graphicKey, Animation animation) {
		this.graphics.put(graphicKey.hashGraphicKey(), animation);
	}

	public Animation getGraphic(IF_GraphicKey graphicKey) {
		Animation graphic = this.graphics.get(graphicKey.hashGraphicKey());
		if (graphic == null) {
			throw new IllegalStateException("Try to access non existing graphic: " + graphicKey.hashGraphicKeyString());
		}
		return graphic;
	}

	public TextureRegion getItemTextureRegion(E_Item item) {
		return this.items[item.ordinal()];
	}

	public TextureRegion getTileTextureRegion(short tileId) {
		return this.tiles.get(tileId);
	}

	public Skin getSkin() {
		return this.skin;
	}

	public Pixmap getCursor() {
		return this.cursor;
	}

	public Texture getBorder() {
		return this.border;
	}

	public Texture getFogOfWar() {
		return this.fogOfWar;
	}

	public BitmapFont getBitmapFont() {
		return this.bitmapFont;
	}

	public ShaderProgram getReplaceColorShaderProgram() {
		return this.replaceColorShaderProgram;
	}

	public ReplaceColorShader getReplaceColorShader() {
		return this.replaceColorShader;
	}

	public static class BasicTileSet extends TileSet {
		public BasicTileSet() {
			super(new File("asset/tiles/tiles.png"));
			this.border = 1;
			this.space = 1;
			this.width = 11;
			this.height = 11;
			this.tileSize = 16;
		}
	}

	private Texture createFogOfWar(int radius, int offset) {
		Pixmap pixmap = new Pixmap(radius * 2, radius * 2, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fillCircle(radius, radius, radius - offset);
		return new Texture(pixmap);
	}

}
