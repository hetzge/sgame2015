package de.hetzge.sgame.setting;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class SystemSettings implements Serializable {

	private String playerName = "Someone";

	private short resolutionX = 800;
	private short resolutionY = 600;

	private float cameraMoveSpeed = 10f;
	private float cameraZoomSpeed = 0.02f;

	private boolean fullscreen = false;

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlayerName(String playerName) {
		if (StringUtils.isEmpty(playerName)) {
			// TODO error
		}
		this.playerName = playerName;
	}

	public short getResolutionX() {
		return this.resolutionX;
	}

	public void setResolutionX(short resolutionX) {
		this.resolutionX = resolutionX;
	}

	public short getResolutionY() {
		return this.resolutionY;
	}

	public void setResolutionY(short resolutionY) {
		this.resolutionY = resolutionY;
	}

	public float getCameraMoveSpeed() {
		return this.cameraMoveSpeed;
	}

	public void setCameraMoveSpeed(float cameraMoveSpeed) {
		this.cameraMoveSpeed = cameraMoveSpeed;
	}

	public float getCameraZoomSpeed() {
		return this.cameraZoomSpeed;
	}

	public void setCameraZoomSpeed(float cameraZoomSpeed) {
		this.cameraZoomSpeed = cameraZoomSpeed;
	}

	public boolean isFullscreen() {
		return this.fullscreen;
	}

	public void setFullscreen(boolean fullscreen) {
		this.fullscreen = fullscreen;
	}

}
