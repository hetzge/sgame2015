package de.hetzge.sgame.setting;

import java.io.Serializable;

public class Player implements Serializable {

	private byte playerId;
	private String playerName;

	public Player() {
	}

	public Player(byte playerId, String playerName) {
		this.playerId = playerId;
		this.playerName = playerName;
	}

	public byte getPlayerId() {
		return playerId;
	}

	public void setPlayerId(byte playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

}
