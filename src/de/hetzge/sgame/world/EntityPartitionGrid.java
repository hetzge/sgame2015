package de.hetzge.sgame.world;

import java.util.List;

import de.hetzge.sgame.entity.Entity;

public class EntityPartitionGrid extends PartitionGrid<Entity> {

	public EntityPartitionGrid(int width, int height, int cellSize) {
		super(width, height, cellSize);
	}

	public List<Entity> getNear(Entity entity) {
		return getNear(entity.getRegisteredX(), entity.getRegisteredY());
	}

}
