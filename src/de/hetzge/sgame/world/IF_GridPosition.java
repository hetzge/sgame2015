package de.hetzge.sgame.world;

import de.hetzge.sgame.misc.E_Orientation;

public interface IF_GridPosition {

	short getGridX();

	short getGridY();

	public default GridPosition getAround(E_Orientation orientation) {
		return new GridPosition((short) (getGridX() + orientation.getOffsetX()), (short) (getGridY() + orientation.getOffsetY()));
	}

}
