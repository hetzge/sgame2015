package de.hetzge.sgame.world;

import java.io.Serializable;

import de.hetzge.sgame.misc.Constant;

public class World implements IF_Grid, Serializable {

	private final short width;
	private final short height;

	private final CollisionGrid fixedCollisionGrid;
	private final TileGrid tileGrid;
	private final ContainerGrid containerGrid;
	private final OwnerGrid ownerGrid;
	private final EntityPartitionGrid entityPartitionGrid;
	private final EntityGrid entityGrid;

	public World(short width, short height) {
		this.width = width;
		this.height = height;

		this.fixedCollisionGrid = new CollisionGrid(width, height);
		this.tileGrid = new TileGrid(width, height);
		this.containerGrid = new ContainerGrid(width, height);
		this.ownerGrid = new OwnerGrid(width, height);
		this.entityPartitionGrid = new EntityPartitionGrid(width, height, 20);
		this.entityGrid = new EntityGrid(width, height, this.entityPartitionGrid);
	}

	@Override
	public short getWidth() {
		return this.width;
	}

	@Override
	public short getHeight() {
		return this.height;
	}

	public CollisionGrid getFixedCollisionGrid() {
		return this.fixedCollisionGrid;
	}

	public TileGrid getTileGrid() {
		return this.tileGrid;
	}

	public ContainerGrid getContainerGrid() {
		return this.containerGrid;
	}

	public OwnerGrid getOwnerGrid() {
		return this.ownerGrid;
	}

	public EntityGrid getEntityGrid() {
		return this.entityGrid;
	}

	public EntityPartitionGrid getEntityPartitionGrid() {
		return this.entityPartitionGrid;
	}

	public int getPixelWidth() {
		return this.width * Constant.TILE_SIZE;
	}

	public int getPixelHeight() {
		return this.height * Constant.TILE_SIZE;
	}

}
