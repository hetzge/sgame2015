package de.hetzge.sgame.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class PartitionGrid<T> implements IF_Grid, Serializable {

	private final int partitionWidth;
	private final int partitionHeight;
	private final int cellSize;
	private final int offset;

	private final List<LinkedHashSet<T>> partitions;

	public PartitionGrid(int width, int height, int cellSize) {
		this.cellSize = cellSize;
		this.offset = (int) Math.floor(cellSize / 2f);
		this.partitionWidth = width / cellSize + 1;
		this.partitionHeight = height / cellSize + 1;
		this.partitions = new ArrayList<>(this.partitionWidth * this.partitionHeight);
		for (int i = 0; i < this.partitionWidth * this.partitionHeight; i++) {
			this.partitions.add(new LinkedHashSet<>());
		}
	}

	public void update(int fromX, int fromY, int toX, int toY, T entry) {
		if (toPartitionXY(fromX) != toPartitionXY(toX) || toPartitionXY(fromY) != toPartitionXY(toY)) {
			unset(fromX, fromY, entry);
			set(toX, toY, entry);
		}
	}

	public void set(int x, int y, T entry) {
		getPartition(x, y).add(entry);
	}

	public void unset(int x, int y, T entry) {
		getPartition(x, y).remove(entry);
	}

	public List<T> getNear(int x, int y) {
		int partitionX = toPartitionXY(x);
		int partitionY = toPartitionXY(y);

		List<T> result = new LinkedList<>();
		for (int ix = partitionX - 1; ix <= partitionX + 1; ix++) {
			for (int iy = partitionY - 1; iy <= partitionY + 1; iy++) {
				if (isOnGrid(ix, iy)) {
					result.addAll(this.partitions.get(index(ix, iy)));
				}
			}
		}

		return result;
	}

	public List<T> get(int x, int y) {
		if (isOnGrid(x, y)) {
			return new ArrayList<>(getPartition(x, y));
		} else {
			return Collections.emptyList();
		}
	}

	private LinkedHashSet<T> getPartition(int x, int y) {
		return this.partitions.get(calculateIndex(x, y));
	}

	private int calculateIndex(int x, int y) {
		return index(toPartitionXY(x), toPartitionXY(y));
	}

	private int toPartitionXY(int x) {
		return (int) Math.floor((x - this.offset) / this.cellSize);
	}

	@Override
	public short getWidth() {
		return (short) this.partitionWidth;
	}

	@Override
	public short getHeight() {
		return (short) this.partitionHeight;
	}

	public static class Tests {

		private static final int X_1 = 0;
		private static final int Y_1 = 0;

		private static final int X_2 = 6;
		private static final int Y_2 = 6;

		private static final String VALUE_A = "A";

		@Test
		public void test() {
			PartitionGrid<String> partitionEntityGrid = new PartitionGrid<>(30, 30, 6);
			partitionEntityGrid.set(X_1, Y_1, VALUE_A);
			Assert.assertThat(partitionEntityGrid.getNear(X_1, Y_1).get(0), CoreMatchers.is(VALUE_A));
			Assert.assertThat(partitionEntityGrid.getNear(X_2, Y_2).get(0), CoreMatchers.is(VALUE_A));
			partitionEntityGrid.unset(X_1, Y_1, VALUE_A);
			Assert.assertThat(partitionEntityGrid.getNear(X_1, Y_1).size(), CoreMatchers.is(0));
			Assert.assertThat(partitionEntityGrid.getNear(X_2, Y_2).size(), CoreMatchers.is(0));
		}

	}

}
