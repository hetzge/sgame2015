package de.hetzge.sgame.world;

import java.io.Serializable;
import java.util.BitSet;

public class CollisionGrid implements IF_Grid, Serializable {

	private final BitSet collision;
	private final short width;
	private final short height;

	public CollisionGrid(short width, short height) {
		this.collision = new BitSet(width * height);
		this.width = width;
		this.height = height;
	}

	@Override
	public short getWidth() {
		return this.width;
	}

	@Override
	public short getHeight() {
		return this.height;
	}

	public void set(boolean[] values) {
		if (values.length != this.width * this.height) {
			throw new IllegalArgumentException("parameter values has not expected length: " + values.length + " from " + this.width * this.height);
		}
		for (int i = 0; i < values.length; i++) {
			if (values[i]) {
				set(i);
			}
		}
	}

	public void set(int index) {
		this.collision.set(index);
	}

	public void set(short x, short y) {
		set(index(x, y));
	}

	public void set(IF_GridPosition gridPosition) {
		set(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public void unset(IF_GridPosition gridPosition) {
		unset(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public void unset(short x, short y) {
		unset(index(x, y));
	}

	public void unset(int index) {
		this.collision.clear(index);
	}

	public boolean is(int index) {
		return this.collision.get(index);
	}

	public boolean is(short x, short y) {
		return is(index(x, y));
	}

	public boolean is(IF_GridPosition gridPosition) {
		return is(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public boolean centeredIs(IF_GridPosition gridPosition, short width, short height) {
		return centeredIs(gridPosition.getGridX(), gridPosition.getGridY(), width, height);
	}

	public boolean centeredIs(short x, short y, short width, short height) {
		return centeredRectGridPositions(x, y, width, height).anyMatch(it -> !isOnGrid(it) || is(it));
	}

	public void setCenteredCollision(short x, short y, short width, short height) {
		if (centeredIs(x, y, width, height)) {
			throw new IllegalStateException("Try to set collision where already is collision. " + x + "|" + y + " and width: " + width + ", height: " + height);
		}
		centeredRectGridPositions(x, y, width, height).forEachOrdered(this::set);
	}

	public void unsetCollision(IF_GridEntity gridEntity) {
		this.unsetCollision(gridEntity.getRegisteredX(), gridEntity.getRegisteredY(), gridEntity.getWidth(), gridEntity.getHeight());
	}

	public void unsetCollision(short x, short y, short width, short height) {
		centeredRectGridPositions(x, y, width, height).forEachOrdered(this::unset);
	}

}