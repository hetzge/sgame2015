package de.hetzge.sgame.world;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.definition.EntityDefinition;
import de.hetzge.sgame.misc.Util;

public class EntityGrid implements IF_Grid, Serializable {

	private final short width;
	private final short height;

	private final Entity[] entities;
	private final EntityPartitionGrid partitionGrid;

	public EntityGrid(short width, short height, EntityPartitionGrid partitionGrid) {
		this.width = width;
		this.height = height;
		this.entities = new Entity[width * height];
		this.partitionGrid = partitionGrid;
	}

	@Override
	public short getWidth() {
		return this.width;
	}

	@Override
	public short getHeight() {
		return this.height;
	}

	public void swap(Entity entityA, Entity entityB) {
		short registeredAX = entityA.getRegisteredX();
		short registeredAY = entityA.getRegisteredY();
		short registeredBX = entityB.getRegisteredX();
		short registeredBY = entityB.getRegisteredY();
		unset0(entityA);
		unset0(entityB);
		set0(registeredBX, registeredBY, entityA);
		set0(registeredAX, registeredAY, entityB);
		updateOnPartition(registeredAX, registeredAY, entityA);
		updateOnPartition(registeredBX, registeredBY, entityB);
	}

	public void set(Entity[] entities) {
		for (Entity entity : entities) {
			if (entity != null) {
				set(entity.getGridX(), entity.getGridY(), entity);
			}
		}
	}

	public void update(IF_GridPosition gridPosition, Entity entity) {
		update(gridPosition.getGridX(), gridPosition.getGridY(), entity);
	}

	public void update(short x, short y, Entity entity) {
		updateOnPartition(x, y, entity);
		unset0(entity);
		set0(x, y, entity);
	}

	private void updateOnPartition(short x, short y, Entity entity) {
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();
		this.partitionGrid.update(registeredX, registeredY, x, y, entity);
	}

	public void set(short x, short y, Entity entity) {
		set0(x, y, entity);
		this.partitionGrid.set(x, y, entity);
	}

	private void set0(short x, short y, Entity entity) {
		EntityDefinition definition = entity.getDefinition();
		short width = definition.getWidth();
		short height = definition.getHeight();
		if (centeredRectGridPositions(x, y, width, height).anyMatch(gridPosition -> is(gridPosition) && !is(gridPosition, entity))) {
			throw new IllegalStateException("Try to move (entity id: " + entity.getId() + ") to already used tile (" + x + "|" + y + ").");
		}
		centeredRectGridPositions(x, y, width, height).forEachOrdered(gridPosition -> setEntity(gridPosition.getGridX(), gridPosition.getGridY(), entity));
		entity.setRegisteredGridPosition(x, y);
	}

	/**
	 * This method is private to guarantee the integrity with
	 * {@link #partitionGrid}
	 */
	private void setEntity(short x, short y, Entity entity) {
		this.entities[Util.index(x, y, this.width)] = entity;
	}

	public void unset(Entity entity) {
		unsetOnPartition(entity);
		unset0(entity);
	}

	private void unsetOnPartition(Entity entity) {
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();

		this.partitionGrid.unset(registeredX, registeredY, entity);
	}

	private void unset0(Entity entity) {
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();

		if (registeredX == -1 || registeredY == -1) {
			return;
		}

		EntityDefinition definition = entity.getDefinition();
		short width = definition.getWidth();
		short height = definition.getHeight();

		centeredRectGridPositions(registeredX, registeredY, width, height).forEachOrdered(this::unset);
	}

	/**
	 * This method is private to guarantee the integrity with
	 * {@link #partitionGrid}
	 */
	private void unset(IF_GridPosition gridPosition) {
		this.entities[index(gridPosition.getGridX(), gridPosition.getGridY())] = null;
	}

	public boolean is(short x, short y) {
		return get(x, y) != null;
	}

	public boolean is(IF_GridPosition gridPosition) {
		return is(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public boolean is(short x, short y, Entity entity) {
		return entity.equals(get(x, y));
	}

	public boolean is(IF_GridPosition gridPosition, Entity entity) {
		return is(gridPosition.getGridX(), gridPosition.getGridY(), entity);
	}

	public boolean centeredIs(short x, short y, short width, short height) {
		return centeredRectGridPositions(x, y, width, height).anyMatch(it -> !isOnGrid(it) || get(it) != null);
	}

	public boolean isNot(IF_GridPosition gridPosition) {
		return !is(gridPosition);
	}

	public boolean isEntity(short x, short y, Entity entity) {
		Entity entityOnGrid = get(x, y);
		if (entityOnGrid != null) {
			return entityOnGrid.equals(entity);
		} else {
			return false;
		}
	}

	public Entity get(short x, short y) {
		return this.entities[index(x, y)];
	}

	public Entity get(IF_GridPosition gridPosition) {
		return get(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public Stream<Entity> centeredGet(short x, short y, short width, short height) {
		return centeredRectGridPositions(x, y, width, height).map(this::get);
	}

	public boolean isEmpty(short x, short y) {
		return get(x, y) == null;
	}

	public Set<Entity> getAllEntities() {
		Set<Entity> result = new HashSet<>();
		for (Entity entity : this.entities) {
			if (entity != null) {
				result.add(entity);
			}
		}
		return result;
	}

	public Entity[] getEntities() {
		return this.entities;
	}

}
