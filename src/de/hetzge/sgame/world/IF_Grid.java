package de.hetzge.sgame.world;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.E_Orientation;
import de.hetzge.sgame.misc.Util;

public interface IF_Grid {

	short getWidth();

	short getHeight();

	public default int index(IF_GridPosition gridPosition) {
		return index(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public default int index(short x, short y) {
		return Util.index(x, y, getWidth());
	}

	public default int index(int x, int y) {
		return Util.index(x, y, getWidth());
	}

	public default boolean isOnGrid(IF_GridPosition gridPosition) {
		return isOnGrid(gridPosition.getGridX(), gridPosition.getGridY());
	}

	public default boolean isOnGrid(int x, int y) {
		return isOnGridX(x) && isOnGridY(y);
	}

	public default boolean isOnGridX(int x) {
		return x >= 0 && x < getWidth();
	}

	public default boolean isOnGridY(int y) {
		return y >= 0 && y < getHeight();
	}

	public default short toGridX(float value) {
		return (short) Math.min(Math.max(value / Constant.TILE_SIZE, 0), getWidth() - 1);
	}

	public default short toGridY(float value) {
		return (short) Math.min(Math.max(value / Constant.TILE_SIZE, 0), getHeight() - 1);
	}

	public default void flood(List<IF_GridPosition> rootPositions, Predicate<IF_GridPosition> visitor) {
		HashSet<IF_GridPosition> visited = new HashSet<>(rootPositions);
		List<List<IF_GridPosition>> nextLists = Arrays.asList(rootPositions);
		List<List<IF_GridPosition>> nextNextLists = new LinkedList<>();

		while (!nextLists.isEmpty()) {
			for (List<IF_GridPosition> nextList : nextLists) {
				List<IF_GridPosition> result = flood0(nextList, visited, visitor);
				if (!result.isEmpty()) {
					nextNextLists.add(result);
				}
			}
			nextLists = nextNextLists;
			nextNextLists = new LinkedList<>();
		}
	}

	default List<IF_GridPosition> flood0(List<IF_GridPosition> positions, Set<IF_GridPosition> visited, Predicate<IF_GridPosition> visitor) {
		List<IF_GridPosition> nexts = new LinkedList<>();
		for (IF_GridPosition gridPosition : positions) {

			if (visitor.test(gridPosition)) {
				for (E_Orientation orientation : E_Orientation.values) {
					IF_GridPosition around = gridPosition.getAround(orientation);
					if (isOnGrid(around) && !visited.contains(around)) {
						visited.add(around);
						nexts.add(around);
					}
				}
			}
		}
		return nexts;
	}

	public default Stream<IF_GridPosition> centeredRectGridPositionsOnGrid(short x, short y, short width, short height) {
		return centeredRectGridPositions(x, y, width, height).filter(this::isOnGrid);
	}

	public default Stream<IF_GridPosition> centeredRectGridPositions(short x, short y, short width, short height) {
		return IntStream.range(0, width * height).mapToObj(it -> {
			int a = x - Util.offset(width) + Util.unIndexX(it, width);
			int b = y - Util.offset(height) + Util.unIndexY(it, width);
			return new GridPosition(a, b);
		});
	}

	public default List<IF_GridPosition> getAroundOnMap(short x, short y) {
		List<IF_GridPosition> result = new ArrayList<>(4);
		IF_GridPosition gridPosition = new GridPosition(x, y);
		for (E_Orientation orientation : E_Orientation.values) {
			IF_GridPosition around = gridPosition.getAround(orientation);
			if (isOnGrid(around)) {
				result.add(around);
			}
		}
		return result;
	}

	public default Iterator<IF_GridPosition> getSpiralIterator(short x, short y, short limit) {
		return new Iterator<IF_GridPosition>() {

			final GridPosition gridPosition = new GridPosition(x, y);

			/**
			 * How many times the phase has repeated
			 */
			short adderI = 0;

			/**
			 * How many times the phase repeats
			 */
			short adder = 1;

			/**
			 * Main counter: counts every step
			 */
			short i1 = 0;

			/**
			 * Phase counter: counts the phases
			 */
			short i2 = 0;

			@Override
			public boolean hasNext() {
				IF_GridPosition next;
				while ((next = searchNext()) == null && i1 <= limit) {
				}
				return next != null;
			}

			@Override
			public IF_GridPosition next() {

				return gridPosition;
			}

			private IF_GridPosition searchNext() {
				int orientationOrdinal = i2 % E_Orientation.values.length;
				E_Orientation orientation = E_Orientation.values[orientationOrdinal];

				gridPosition.set((short) (gridPosition.getGridX() + orientation.getOffsetX()), (short) (gridPosition.getGridY() + orientation.getOffsetY()));

				i1++;
				adderI++;

				if (adderI == adder) {
					if (i2 % 2 == 1) {
						adder++;
					}
					i2++;
					adderI = 0;
				}

				if (isOnGrid(gridPosition)) {
					return gridPosition;
				} else {
					return null;
				}
			}

		};
	}

}
