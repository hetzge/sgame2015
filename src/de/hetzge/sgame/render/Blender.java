package de.hetzge.sgame.render;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public final class Blender {

	private final int source;
	private final int destination;

	private int sourceBackup;
	private int destinationBackup;

	public Blender(int source, int destination) {
		this.source = source;
		this.destination = destination;
	}

	public void start(SpriteBatch spriteBatch) {
		sourceBackup = spriteBatch.getBlendSrcFunc();
		destinationBackup = spriteBatch.getBlendDstFunc();

		spriteBatch.enableBlending();
		spriteBatch.setBlendFunction(source, destination);
	}

	public void end(SpriteBatch spriteBatch) {
		spriteBatch.setBlendFunction(sourceBackup, destinationBackup);
	}

}
