package de.hetzge.sgame.render;

import java.util.List;
import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import de.hetzge.sgame.App;
import de.hetzge.sgame.booking.Container;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;
import de.hetzge.sgame.entity.job.IF_RenderItemsJob;
import de.hetzge.sgame.item.E_Item;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.Util;
import de.hetzge.sgame.render.shader.ReplaceColorShader;

public class EntityRenderer implements IF_Renderer {

	public void render() {
		getSpriteBatch().begin();
		getShapeRenderer().begin();

		List<Entity> entities = App.worldRenderer.getVisibleEntities();
		for (Entity entity : entities) {
			render(entity);
		}

		Set<Entity> selection = App.getGame().getLocalGameState().getEntitySelections().getSelections();
		for (Entity entity : selection) {
			App.entityRenderer.renderSelected(entity);
		}

		getSpriteBatch().end();
		getShapeRenderer().end();
	}

	public void render(Entity entity) {
		float stateTime = App.gdxTiming.getStateTime();

		Animation animation = App.ressources.getGraphic(entity);
		TextureRegion keyFrame = animation.getKeyFrame(stateTime, true);

		ReplaceColorShader replaceColorShader = App.ressources.getReplaceColorShader();
		byte owner = entity.getOwner();
		if (owner > -1) {
			Color color = Constant.COLORS[owner];

			replaceColorShader.begin();
			replaceColorShader.setReplaceColor(Constant.DEFAULT_REPLACEMENT_COLOR);
			replaceColorShader.setReplaceWithColor(color);
		}

		float renderWidth = entity.getRenderWidth();
		float renderHeight = entity.getRenderHeight();

		float renderX = entity.getRenderX();
		float renderY = entity.getRenderY();
		getSpriteBatch().draw(keyFrame, renderX, -renderY, renderWidth, renderHeight);

		replaceColorShader.end();

		renderItems(entity);
		renderItem(entity);
		renderId(entity);
		renderPath(entity);
		renderRegistration(entity);
		renderDoor(entity);
	}

	private void renderSelected(Entity entity) {
		float renderX = entity.getRenderX();
		float renderY = entity.getRenderY();
		float width = Util.gridToWorld(entity.getDefinition().getWidth());
		ShapeRenderer shapeRenderer = getShapeRenderer();
		shapeRenderer.setColor(Color.RED);
		shapeRenderer.line(renderX, -renderY, renderX + width, -renderY);
	}

	private void renderItems(Entity entity) {
		EntityJob job = entity.getJob();
		if (job instanceof IF_RenderItemsJob) {
			IF_RenderItemsJob renderItemsJob = (IF_RenderItemsJob) job;
			Container<E_Item> left = renderItemsJob.getRenderLeftContainer();
			Container<E_Item> right = renderItemsJob.getRenderRightContainer();

			short doorX = entity.getDoorX();
			short doorY = entity.getDoorY();

			float doorXLeft = doorX * Constant.TILE_SIZE;
			float doorYLeft = doorY * Constant.TILE_SIZE;

			float doorXRight = doorXLeft + Constant.TILE_SIZE;
			float doorYRight = doorYLeft;

			if (left != null) {
				App.itemRenderer.renderContainer(left, doorXLeft, doorYLeft);
			}
			if (right != null) {
				App.itemRenderer.renderContainer(right, doorXRight, doorYRight);
			}
		}
	}

	private void renderItem(Entity entity) {
		E_Item item = entity.getItem();
		if (item != null) {
			float renderX = entity.getRenderX();
			float renderY = entity.getRenderY();
			TextureRegion itemTextureRegion = App.ressources.getItemTextureRegion(item);
			getSpriteBatch().draw(itemTextureRegion, renderX, -renderY);
		}
	}

	private void renderId(Entity entity) {
		if (App.getGame().getLocalGameState().isShowIds()) {
			float renderX = entity.getRenderX();
			float renderY = entity.getRenderY();
			int id = entity.getId();
			getShapeRenderer().setColor(Color.WHITE);
			getBitmapFont().draw(getSpriteBatch(), id + "", renderX, -renderY);
		}
	}

	private void renderPath(Entity entity) {
		if (App.getGame().getLocalGameState().isShowPaths() && entity.hasPath()) {
			short pathGoalX = entity.getPathGoalX();
			short pathGoalY = entity.getPathGoalY();
			float renderX = entity.getRenderX();
			float renderY = entity.getRenderY();
			getShapeRenderer().setColor(Color.WHITE);
			getShapeRenderer().line(renderX, -renderY, pathGoalX * Constant.TILE_SIZE, -pathGoalY * Constant.TILE_SIZE);
		}
	}

	private void renderRegistration(Entity entity) {
		if (App.getGame().getLocalGameState().isShowRegistrations()) {
			short registeredX = entity.getRegisteredX();
			short registeredY = entity.getRegisteredY();
			float renderX = entity.getRenderX();
			float renderY = entity.getRenderY();
			int goalRenderX = registeredX * Constant.TILE_SIZE;
			int goalRenderY = registeredY * Constant.TILE_SIZE;
			getShapeRenderer().setColor(Color.RED);
			getShapeRenderer().line(renderX, -renderY, goalRenderX, -goalRenderY);
		}
	}

	private void renderDoor(Entity entity) {
		if (App.getGame().getLocalGameState().isShowDoors()) {
			if (!entity.getDefinition().isMoveable()) {
				short doorX = entity.getDoorX();
				short doorY = entity.getDoorY();
				getShapeRenderer().setColor(Color.GREEN);
				short width = Constant.TILE_SIZE;
				short height = Constant.TILE_SIZE;
				int renderX = doorX * Constant.TILE_SIZE;
				int renderY = doorY * Constant.TILE_SIZE + height;
				getShapeRenderer().rect(renderX, -renderY, width, height);
			}
		}
	}

	public void renderFogOfWar() {

		if (App.getGame().getLocalGameState().isShowFogOfWar()) {

			Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);

			Gdx.gl.glDepthMask(true);
			Gdx.gl.glColorMask(false, false, false, false);
			Gdx.gl.glDepthFunc(GL20.GL_ALWAYS);

			getShapeRenderer().begin(ShapeType.Filled);

			List<Entity> entities = App.worldRenderer.getVisibleEntities();
			for (Entity entity : entities) {
				if (App.entityFunction.isOwnEntity(entity)) {
					getShapeRenderer().rect(entity.getRenderX() - 50, -(entity.getRenderY() + 50), 300, 300);
				}
			}

			getShapeRenderer().end();

			Gdx.gl.glDepthFunc(GL20.GL_EQUAL);
			Gdx.gl.glColorMask(true, true, true, true);
		} else {
			Gdx.gl.glDepthFunc(GL20.GL_ALWAYS);
		}

	}

}
