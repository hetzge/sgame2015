package de.hetzge.sgame.function;

import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.world.Path;

public interface IF_Search<BY> {
	public <SEARCH_RESULT> Pair<SEARCH_RESULT, Path> findAndPath(Entity entity, java.util.function.Function<BY, SEARCH_RESULT> searchFunction);

	public <SEARCH_RESULT> SEARCH_RESULT find(Entity entity, java.util.function.Function<BY, SEARCH_RESULT> searchFunction);

	public Path findPath(Entity entity, Predicate<BY> goalPredicate);
}