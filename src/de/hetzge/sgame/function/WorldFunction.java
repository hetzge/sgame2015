package de.hetzge.sgame.function;

import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.definition.EntityDefinition;
import de.hetzge.sgame.entity.definition.EntityDefinition.Landmark;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.world.CollisionGrid;
import de.hetzge.sgame.world.EntityGrid;
import de.hetzge.sgame.world.IF_GridPosition;
import de.hetzge.sgame.world.OwnerGrid;
import de.hetzge.sgame.world.World;

public class WorldFunction {

	public boolean isAtHome(Entity entity) {
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();
		EntityDefinition definition = entity.getDefinition();
		short width = definition.getWidth();
		short height = definition.getHeight();
		byte owner = entity.getOwner();

		OwnerGrid ownerGrid = App.getGame().getWorld().getOwnerGrid();

		return ownerGrid.centeredIs(registeredX, registeredY, width, height, owner);
	}

	public boolean checkSpaceForEntity(Entity entity, short x, short y) {
		return checkSpaceForEntity(entity.getEntityType(), x, y);
	}

	public boolean checkSpaceForEntity(E_EntityType entityType, short x, short y) {
		return checkSpaceForEntity(Byte.MIN_VALUE, entityType, x, y);
	}

	/**
	 * Checks if owner-, fixed- and entity-grid is empty for the given
	 * {@link E_EntityType}
	 * 
	 * @param owner
	 *            if this is <code>Byte.MIN_VALUE</code> then the ownership will
	 *            be ignored
	 */
	public boolean checkSpaceForEntity(byte owner, E_EntityType entityType, short x, short y) {
		World world = App.getGame().getWorld();
		EntityGrid entityGrid = App.getGame().getWorld().getEntityGrid();
		CollisionGrid fixedCollisionGrid = world.getFixedCollisionGrid();
		OwnerGrid ownerGrid = world.getOwnerGrid();

		EntityDefinition entityDefinition = entityType.getEntityDefinition();
		short width = entityDefinition.getWidth();
		short height = entityDefinition.getHeight();

		if (owner != Byte.MIN_VALUE && !ownerGrid.centeredIs(x, y, width, height, owner)) {
			return false;
		}

		if (fixedCollisionGrid.centeredIs(x, y, width, height)) {
			return false;
		}

		if (entityGrid.centeredIs(x, y, width, height)) {
			return false;
		}

		return true;
	}

	public boolean checkSpace(IF_GridPosition gridPosition, Entity entity) {
		return checkSpace(gridPosition, entity.getOwner(), entity.getEntityType());
	}

	public boolean checkSpace(IF_GridPosition gridPosition, byte owner, E_EntityType entityType) {
		return checkSpace(gridPosition.getGridX(), gridPosition.getGridY(), owner, entityType);
	}

	/**
	 * Checks if there is space for the given <code>entityType</code> at the
	 * given position (<code>x</code>, <code>y</code>) at the moment. Movable
	 * collision is considered.
	 * 
	 * @see #isEntityPossible(IF_GridPosition, Entity)
	 */
	public boolean checkSpace(short x, short y, byte owner, E_EntityType entityType) {
		World world = App.getGame().getWorld();
		EntityGrid entityGrid = App.getGame().getWorld().getEntityGrid();
		CollisionGrid fixedCollisionGrid = world.getFixedCollisionGrid();
		OwnerGrid ownerGrid = world.getOwnerGrid();
		EntityDefinition definition = entityType.getEntityDefinition();

		if (owner != Byte.MIN_VALUE && definition.isBordered() && !ownerGrid.is(x, y, owner)) {
			return false;
		}

		if (fixedCollisionGrid.is(x, y)) {
			return false;
		}

		if (entityGrid.is(x, y)) {
			return false;
		}

		return true;
	}

	/**
	 * Checks if the given <code>entity</code> could be theoretically at given
	 * <code>gridPosition</code>. Movable collision is not considered.
	 * 
	 * @see #checkSpace(short, short, byte, E_EntityType)
	 */
	public boolean isEntityPossible(IF_GridPosition gridPosition, Entity entity) {
		World world = App.getGame().getWorld();
		CollisionGrid fixedCollisionGrid = world.getFixedCollisionGrid();
		OwnerGrid ownerGrid = world.getOwnerGrid();

		EntityDefinition definition = entity.getDefinition();

		if (definition.isBordered()) {
			byte owner = entity.getOwner();

			if (!ownerGrid.is(gridPosition, owner)) {
				return false;
			}
		}

		if (fixedCollisionGrid.is(gridPosition)) {
			return false;
		}

		return true;
	}

	public IF_GridPosition findEmptyGridPositionAround(E_EntityType entityType, short x, short y) {
		EntityDefinition entityDefinition = entityType.getEntityDefinition();
		boolean isMoveable = entityDefinition.isMoveable();
		if (!isMoveable) {
			throw new IllegalArgumentException("This only works with moveable entities");
		}

		World world = App.getGame().getWorld();
		CollisionGrid fixedCollisionGrid = world.getFixedCollisionGrid();
		EntityGrid entityGrid = world.getEntityGrid();

		Iterator<IF_GridPosition> spiralIterator = world.getSpiralIterator(x, y, (short) 30);
		while (spiralIterator.hasNext()) {
			IF_GridPosition gridPosition = spiralIterator.next();
			boolean isFixedCollision = fixedCollisionGrid.is(gridPosition);
			boolean isEntityCollision = entityGrid.is(gridPosition);
			if (!isFixedCollision && !isEntityCollision) {
				return gridPosition;
			}
		}
		return null;
	}

	public void conquerTerritory(Entity entity) {
		short diameter = (short) (entity.getDefinition().getBorderRadius() * 2);
		World world = App.getGame().getWorld();
		OwnerGrid ownerGrid = world.getOwnerGrid();
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();
		byte owner = entity.getOwner();
		Stream<IF_GridPosition> centeredRectGridPositions = ownerGrid.centeredRectGridPositions(registeredX, registeredY, diameter, diameter);
		centeredRectGridPositions.filter(ownerGrid::isOnGrid).filter(gridPosition -> ownerGrid.is(gridPosition, Constant.GAIA_PLAYER_ID)).forEachOrdered(gridPosition -> {
			ownerGrid.setOwnership(gridPosition, owner);
		});
	}

	public void loseTerritory(Entity entity) {
		short diameter = (short) (entity.getDefinition().getBorderRadius() * 2);
		World world = App.getGame().getWorld();
		OwnerGrid ownerGrid = world.getOwnerGrid();
		EntityGrid entityGrid = world.getEntityGrid();
		short registeredX = entity.getRegisteredX();
		short registeredY = entity.getRegisteredY();
		byte owner = ownerGrid.getOwnership(registeredX, registeredY);

		// neutralize owner
		ownerGrid.centeredRectGridPositionsOnGrid(registeredX, registeredY, diameter, diameter).filter(gridPosition -> ownerGrid.is(gridPosition, owner)).forEachOrdered(gridPosition -> {
			ownerGrid.setOwnership(gridPosition, Constant.GAIA_PLAYER_ID);
		});

		// recalculate landmarks
		recalculateLandmarks();

		// destroy entities
		entityGrid.centeredRectGridPositionsOnGrid(registeredX, registeredY, diameter, diameter).map(it -> entityGrid.get(it)).filter(Objects::nonNull).distinct().filter(it -> !(it.getDefinition() instanceof Landmark)).filter(it -> it.getOwner() != Constant.GAIA_PLAYER_ID).filter(it -> !isAtHome(it)).filter(it -> it.getDefinition().isBordered()).forEachOrdered(it -> App.entityFunction.destroyEntity(it));
	}

	public void recalculateLandmarks() {
		App.getGame().getEntityManager().getEntities().stream().filter(it -> it.getDefinition() instanceof Landmark).filter(it -> it.getActivity() != E_Activity.DESTROY).sorted((it, other) -> it.getId() - other.getId()).forEachOrdered(it -> App.worldFunction.conquerTerritory(it));
	}

}
