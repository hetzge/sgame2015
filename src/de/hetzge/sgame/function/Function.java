package de.hetzge.sgame.function;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.Players;
import de.hetzge.sgame.game.event.EventPlayerHandshake;
import de.hetzge.sgame.game.format.GameFormat;
import de.hetzge.sgame.game.format.GameFormat.ContainerFormat;
import de.hetzge.sgame.game.format.GameFormat.EntityFormat;
import de.hetzge.sgame.item.E_Item;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.Util;
import de.hetzge.sgame.network.Network;
import de.hetzge.sgame.setting.Player;
import de.hetzge.sgame.setting.SystemSettings;
import de.hetzge.sgame.world.World;

public class Function {

	public void initGame(GameFormat gameFormat) {
		Logger.info("init game");

		int worldWidth = gameFormat.getWorldWidth();
		int worldHeight = gameFormat.getWorldHeight();
		short[] tiles = gameFormat.getTiles();
		boolean[] collision = gameFormat.getCollision();
		EntityFormat[] entityFormats = gameFormat.getEntities();
		ContainerFormat[][] containerFormatss = gameFormat.getContainers();

		SystemSettings systemSettings = App.settings.getSystemSettings();
		Players players = new Players();
		Player playerSettings = new Player(players.nextPlayerId(), systemSettings.getPlayerName());
		players.addPlayer(playerSettings);

		World world = new World((short) worldWidth, (short) worldHeight);
		world.getTileGrid().set(tiles);
		world.getFixedCollisionGrid().set(collision);
		App.getGame().setWorld(world);
		App.getGame().setSelf(playerSettings);
		App.getGame().setPlayers(players);

		// for (int i = 0; i < 40; i++) {
		// for (int j = 0; j < 40; j++) {
		// App.getGame().getWorld().getOwnerGrid().setOwnership(i, j, (byte) 1);
		// }
		// }
		//
		// for (int i = 0; i < 20; i++) {
		// for (int j = 0; j < 20; j++) {
		// App.getGame().getWorld().getOwnerGrid().setOwnership(i, j, (byte) 0);
		// }
		// }

		for (int index = 0; index < entityFormats.length; index++) {
			EntityFormat entityFormat = entityFormats[index];
			if (entityFormat != null) {
				int x = Util.unIndexX(index, worldWidth);
				int y = Util.unIndexY(index, worldWidth);
				boolean isEnvironment = entityFormat.getEntityType().isEnvironment();
				byte ownership = isEnvironment ? Constant.GAIA_PLAYER_ID : entityFormat.getOwner();
				App.entityFunction.createEntity(entityFormat.getEntityType(), (short) x, (short) y, App.getGame().getEntityManager().getIdProvider().next(), ownership);
			}
		}

		for (int index = 0; index < containerFormatss.length; index++) {
			ContainerFormat[] containerFormats = containerFormatss[index];
			if (containerFormats != null) {
				int x = Util.unIndexX(index, worldWidth);
				int y = Util.unIndexY(index, worldWidth);
				for (int i = 0; i < containerFormats.length; i++) {
					ContainerFormat containerFormat = containerFormats[i];
					if (containerFormat != null) {
						E_Item item = containerFormat.getItem();
						int count = containerFormat.getCount();
						App.getGame().getWorld().getContainerGrid().get((short) x, (short) y).set(item, count);
					}
				}
			}
		}

	}

	public void quitGame() {
		System.exit(0);
	}

	public void sendHandshake(Network network) {
		Logger.info("send handshake to server");
		network.send(new EventPlayerHandshake(App.settings.getSystemSettings().getPlayerName()));
	}

}
