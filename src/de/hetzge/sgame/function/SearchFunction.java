package de.hetzge.sgame.function;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

import de.hetzge.sgame.App;
import de.hetzge.sgame.astar.AStar;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.world.GridPosition;
import de.hetzge.sgame.world.Path;
import de.hetzge.sgame.world.World;

public class SearchFunction {

	public final IF_Search<Entity> byEntity = new IF_Search<Entity>() {

		@Override
		public <SEARCH_RESULT> Pair<SEARCH_RESULT, Path> findAndPath(Entity entity, Function<Entity, SEARCH_RESULT> entitySearchFunction) {
			GridPosition start = entity.getRegisteredGridPosition();
			return createAStar(entity).find(start, createGridPositionSearchFunction(entitySearchFunction));
		}

		@Override
		public <SEARCH_RESULT> SEARCH_RESULT find(Entity entity, Function<Entity, SEARCH_RESULT> searchFunction) {
			return findAndPath(entity, searchFunction).getLeft();
		}

		@Override
		public Path findPath(Entity entity, Predicate<Entity> entitySearchPredicate) {
			GridPosition start = entity.getRegisteredGridPosition();
			return createAStar(entity).find(start, createGridPositionSearchFunction(entityToTest -> {
				return entitySearchPredicate.test(entityToTest) ? entityToTest : null;
			})).getRight();
		}

		private <SEARCH_RESULT> Function<GridPosition, SEARCH_RESULT> createGridPositionSearchFunction(Function<Entity, SEARCH_RESULT> searchFunction) {
			return gridPosition -> {
				if (!isOnGrid(gridPosition)) {
					return null;
				}
				Entity entityToTest = getEntityOnPosition(gridPosition);
				if (entityToTest == null) {
					return null;
				}
				return searchFunction.apply(entityToTest);
			};
		}

	};

	public final IF_Search<GridPosition> byGridPosition = new IF_Search<GridPosition>() {

		@Override
		public <SEARCH_RESULT> Pair<SEARCH_RESULT, Path> findAndPath(Entity entity, Function<GridPosition, SEARCH_RESULT> searchFunction) {
			GridPosition start = entity.getRegisteredGridPosition();
			return createAStar(entity).find(start, searchFunction);
		}

		@Override
		public <SEARCH_RESULT> SEARCH_RESULT find(Entity entity, Function<GridPosition, SEARCH_RESULT> searchFunction) {
			return findAndPath(entity, searchFunction).getLeft();
		}

		@Override
		public Path findPath(Entity entity, Predicate<GridPosition> goalPredicate) {
			return findAndPath(entity, gridPosition -> goalPredicate.test(gridPosition) ? gridPosition : null).getRight();
		}

	};

	public Map<Entity, Path> findPath(List<Entity> entities, GridPosition goal) {
		Map<Entity, Path> result = new HashMap<>();
		for (Entity entity : entities) {
			result.put(entity, findPath(entity, goal));
		}
		return result;
	}

	public Path findPath(Entity entity, GridPosition goal) {
		GridPosition start = entity.getRegisteredGridPosition();
		return createAStar(entity).findPath(start, goal, Constant.PRIORITY_PATHFINDING);
	}

	public Path findPath(Entity entity, Entity toEntity) {
		if (toEntity.getDefinition().isMoveable()) {
			return createAStar(entity).findPath(entity.getRegisteredGridPosition(), toEntity.getRegisteredGridPosition(), Constant.PRIORITY_PATHFINDING);
		} else {
			return createAStar(entity).findPath(entity.getRegisteredGridPosition(), toEntity.getDoorGridPosition(), Constant.PRIORITY_PATHFINDING);
		}
	}

	private Entity getEntityOnPosition(GridPosition gridPosition) {
		return App.getGame().getWorld().getEntityGrid().get(gridPosition);
	}

	private boolean isOnGrid(GridPosition gridPosition) {
		return App.getGame().getWorld().isOnGrid(gridPosition);
	}

	private AStar createAStar(Entity entity) {
		World world = App.getGame().getWorld();

		short worldWidth = world.getWidth();
		Predicate<GridPosition> onGridPredicate = world.getFixedCollisionGrid()::isOnGrid;
		Predicate<GridPosition> isMoveablePossiblePredicate = (gridPosition) -> App.worldFunction.isEntityPossible(gridPosition, entity);
		return new AStar(onGridPredicate.and(isMoveablePossiblePredicate).negate(), worldWidth);
	}

}
