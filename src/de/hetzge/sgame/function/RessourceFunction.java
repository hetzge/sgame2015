package de.hetzge.sgame.function;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.IntMap;

import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.graphic.GraphicKey;
import de.hetzge.sgame.item.E_Item;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.E_Orientation;
import de.hetzge.sgame.misc.Util;
import de.hetzge.sgame.world.TileSet;

public class RessourceFunction {

	public List<TextureRegion> loadTiles(List<TileSet> tileSets) {
		List<TextureRegion> tiles = new ArrayList<>();

		for (TileSet tileSet : tileSets) {
			FileHandle fileHandle = Gdx.files.internal(tileSet.getFile().getPath());
			if (Constant.IMAGE_FILE_ENDINGS.contains(fileHandle.extension())) {
				Texture texture = new Texture(fileHandle);
				int border = tileSet.getBorder();
				int space = tileSet.getSpace();
				int tileSize = tileSet.getTileSize();
				for (int iy = 0; iy < tileSet.getHeight(); iy++) {
					for (int ix = 0; ix < tileSet.getWidth(); ix++) {
						int x = border + ix * space + ix * tileSize;
						int y = border + iy * space + iy * tileSize;
						tiles.add(new TextureRegion(texture, x, y, tileSize, tileSize));
					}
				}
			} else {
				throw new IllegalArgumentException("Invalid tile set file ending.");
			}
		}

		return tiles;
	}

	public TextureRegion[] loadItems() {
		Texture texture = new Texture(Gdx.files.internal("asset/dummy/item.png"));
		TextureRegion[] items = new TextureRegion[E_Item.values.length];

		items[E_Item.BREAD.ordinal()] = new TextureRegion(texture, 0 * Constant.ITEM_WIDTH, 0 * Constant.ITEM_HEIGHT, Constant.ITEM_WIDTH, Constant.ITEM_HEIGHT);
		items[E_Item.FISCH.ordinal()] = new TextureRegion(texture, 1 * Constant.ITEM_WIDTH, 0 * Constant.ITEM_HEIGHT, Constant.ITEM_WIDTH, Constant.ITEM_HEIGHT);
		items[E_Item.STONE.ordinal()] = new TextureRegion(texture, 2 * Constant.ITEM_WIDTH, 0 * Constant.ITEM_HEIGHT, Constant.ITEM_WIDTH, Constant.ITEM_HEIGHT);
		items[E_Item.WOOD.ordinal()] = new TextureRegion(texture, 3 * Constant.ITEM_WIDTH, 0 * Constant.ITEM_HEIGHT, Constant.ITEM_WIDTH, Constant.ITEM_HEIGHT);

		return items;
	}

	public IntMap<Animation> loadGraphics() {
		IntMap<Animation> result = new IntMap<>();

		Texture buildingTexture = new Texture(Gdx.files.internal("asset/dummy/building.png"));
		Texture carrierTexture = new Texture(Gdx.files.internal("asset/dummy/carrier.png"));
		Texture workerTexture = new Texture(Gdx.files.internal("asset/dummy/worker.png"));
		Texture fighterTexture = new Texture(Gdx.files.internal("asset/dummy/fighter.png"));
		Texture environmentTexture = new Texture(Gdx.files.internal("asset/dummy/environment.png"));

		TextureRegion[][] carrierTextureRegions = TextureRegion.split(carrierTexture, 24, 24);
		TextureRegion[][] workerTextureRegions = TextureRegion.split(workerTexture, 24, 24);
		TextureRegion[][] fighterTextureRegions = TextureRegion.split(fighterTexture, 24, 24);

		TextureRegion treeTextureRegion = new TextureRegion(environmentTexture, 0, 0, 48, 48);
		TextureRegion stoneTextureRegion = new TextureRegion(environmentTexture, 24, 48, 24, 24);
		TextureRegion buildingATextureRegion = new TextureRegion(buildingTexture, 0, 0, 48, 48);
		TextureRegion buildingBTextureRegion = new TextureRegion(buildingTexture, 0, 48, 48, 48);
		TextureRegion buildingLotTextureRegion = new TextureRegion(buildingTexture, 0, 96, 48, 48);
		TextureRegion landmarkTextureRegion = new TextureRegion(buildingTexture, 0, 144, 24, 24);

		for (E_EntityType entityType : E_EntityType.values) {
			for (E_Orientation orientation : E_Orientation.values) {
				result.put(new GraphicKey(orientation, E_Activity.DESTROY, entityType).hashGraphicKey(), Util.textureRegionToAnimation(treeTextureRegion));
			}
		}

		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.TREE).hashGraphicKey(), Util.textureRegionToAnimation(treeTextureRegion));
		result.put(new GraphicKey(E_Activity.DESTROY, E_EntityType.TREE).hashGraphicKey(), Util.textureRegionToAnimation(treeTextureRegion));
		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.CAIRN).hashGraphicKey(), Util.textureRegionToAnimation(stoneTextureRegion));
		result.put(new GraphicKey(E_Activity.DESTROY, E_EntityType.CAIRN).hashGraphicKey(), Util.textureRegionToAnimation(stoneTextureRegion));
		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.BUILDING_LUMBERJACK).hashGraphicKey(), Util.textureRegionToAnimation(buildingATextureRegion));
		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.BUILDING_QUARRY).hashGraphicKey(), Util.textureRegionToAnimation(buildingBTextureRegion));
		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.BUILDING_FACTORY).hashGraphicKey(), Util.textureRegionToAnimation(buildingBTextureRegion));
		result.put(new GraphicKey(E_Activity.WORKING, E_EntityType.BUILDING_FACTORY).hashGraphicKey(), Util.textureRegionToAnimation(buildingBTextureRegion));
		result.put(new GraphicKey(E_Activity.IDLE, E_EntityType.LANDMARK).hashGraphicKey(), Util.textureRegionToAnimation(landmarkTextureRegion));
		result.put(new GraphicKey(E_Activity.DESTROY, E_EntityType.LANDMARK).hashGraphicKey(), Util.textureRegionToAnimation(landmarkTextureRegion));

		for (E_EntityType buildingLotEntityType : E_EntityType.BUILDING_LOTS) {
			result.put(new GraphicKey(E_Activity.IDLE, buildingLotEntityType).hashGraphicKey(), Util.textureRegionToAnimation(buildingLotTextureRegion));
		}

		loadUnit(result, E_EntityType.WORKER_LUMBERJACK, workerTextureRegions);
		loadUnit(result, E_EntityType.WORKER_MASON, workerTextureRegions);
		loadUnit(result, E_EntityType.CARRIER, carrierTextureRegions);
		loadUnit(result, E_EntityType.FIGHTER, fighterTextureRegions);

		return result;
	}

	private void loadUnit(IntMap<Animation> result, E_EntityType type, TextureRegion[][] textureRegions) {
		Animation idleEastAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[0][0] });
		Animation idleSouthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[1][0] });
		Animation idleNorthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[2][0] });
		Animation idleWestAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[3][0] });

		Animation walkEastAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[0][0], textureRegions[0][1] });
		Animation walkSouthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[1][0], textureRegions[1][1] });
		Animation walkNorthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[2][0], textureRegions[2][1] });
		Animation walkWestAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[3][0], textureRegions[3][1] });

		Animation workEastAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[4][0], textureRegions[4][1] });
		Animation workSouthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[5][0], textureRegions[5][1] });
		Animation workNorthAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[6][0], textureRegions[6][1] });
		Animation workWestAnimation = new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegions[7][0], textureRegions[7][1] });

		result.put(new GraphicKey(E_Orientation.EAST, E_Activity.IDLE, type).hashGraphicKey(), idleEastAnimation);
		result.put(new GraphicKey(E_Orientation.SOUTH, E_Activity.IDLE, type).hashGraphicKey(), idleSouthAnimation);
		result.put(new GraphicKey(E_Orientation.NORTH, E_Activity.IDLE, type).hashGraphicKey(), idleNorthAnimation);
		result.put(new GraphicKey(E_Orientation.WEST, E_Activity.IDLE, type).hashGraphicKey(), idleWestAnimation);

		result.put(new GraphicKey(E_Orientation.EAST, E_Activity.WALKING, type).hashGraphicKey(), walkEastAnimation);
		result.put(new GraphicKey(E_Orientation.SOUTH, E_Activity.WALKING, type).hashGraphicKey(), walkSouthAnimation);
		result.put(new GraphicKey(E_Orientation.NORTH, E_Activity.WALKING, type).hashGraphicKey(), walkNorthAnimation);
		result.put(new GraphicKey(E_Orientation.WEST, E_Activity.WALKING, type).hashGraphicKey(), walkWestAnimation);

		result.put(new GraphicKey(E_Orientation.EAST, E_Activity.CARRY, type).hashGraphicKey(), walkEastAnimation);
		result.put(new GraphicKey(E_Orientation.SOUTH, E_Activity.CARRY, type).hashGraphicKey(), walkSouthAnimation);
		result.put(new GraphicKey(E_Orientation.NORTH, E_Activity.CARRY, type).hashGraphicKey(), walkNorthAnimation);
		result.put(new GraphicKey(E_Orientation.WEST, E_Activity.CARRY, type).hashGraphicKey(), walkWestAnimation);

		result.put(new GraphicKey(E_Orientation.EAST, E_Activity.WORKING, type).hashGraphicKey(), workEastAnimation);
		result.put(new GraphicKey(E_Orientation.SOUTH, E_Activity.WORKING, type).hashGraphicKey(), workSouthAnimation);
		result.put(new GraphicKey(E_Orientation.NORTH, E_Activity.WORKING, type).hashGraphicKey(), workNorthAnimation);
		result.put(new GraphicKey(E_Orientation.WEST, E_Activity.WORKING, type).hashGraphicKey(), workWestAnimation);
	}

}
