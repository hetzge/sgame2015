package de.hetzge.sgame.function;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.world.GridPosition;
import de.hetzge.sgame.world.Path;

public class AsyncSearchService {

	private LinkedList<Runnable> queue = new LinkedList<>();
	private ExecutorService executorService = Executors.newFixedThreadPool(Constant.PATHFINDING_THREADS);

	public final ByEntity byEntity = new ByEntity();
	public final ByGridPosition byGridPosition = new ByGridPosition();

	public AsyncSearchService() {
	}

	public void update() {

		List<Future<?>> futures = new LinkedList<>();

		for (int i = 0; i < Constant.PATHFINDING_EVENTS_PER_FRAME; i++) {
			if (this.queue.isEmpty()) {
				break;
			}
			Runnable pop = this.queue.pop();
			futures.add(this.executorService.submit(pop));
		}

		for (Future<?> future : futures) {
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new IllegalStateException(e);
			}
		}
	}

	public class ByEntity {

		public <SEARCH_RESULT> void findAndPath(Entity entity, Function<Entity, SEARCH_RESULT> searchFunction, Consumer<Pair<SEARCH_RESULT, Path>> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byEntity.findAndPath(entity, searchFunction));
			});
		}

		public <SEARCH_RESULT> void find(Entity entity, Function<Entity, SEARCH_RESULT> searchFunction, Consumer<SEARCH_RESULT> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byEntity.find(entity, searchFunction));
			});

		}

		public void findPath(Entity entity, Predicate<Entity> goalPredicate, Consumer<Path> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byEntity.findPath(entity, goalPredicate));
			});
		}
	}

	public class ByGridPosition {

		public <SEARCH_RESULT> void findAndPath(Entity entity, Function<GridPosition, SEARCH_RESULT> searchFunction, Consumer<Pair<SEARCH_RESULT, Path>> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byGridPosition.findAndPath(entity, searchFunction));
			});
		}

		public <SEARCH_RESULT> void find(Entity entity, Function<GridPosition, SEARCH_RESULT> searchFunction, Consumer<SEARCH_RESULT> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byGridPosition.find(entity, searchFunction));
			});
		}

		public void findPath(Entity entity, Predicate<GridPosition> goalPredicate, Consumer<Path> callback) {
			AsyncSearchService.this.queue.add(() -> {
				callback.accept(App.searchFunction.byGridPosition.findPath(entity, goalPredicate));
			});
		}

	}

}
