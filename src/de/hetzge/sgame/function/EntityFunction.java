package de.hetzge.sgame.function;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.definition.EntityDefinition;
import de.hetzge.sgame.entity.job.EntityJob;
import de.hetzge.sgame.entity.job.main.DestroyJob;
import de.hetzge.sgame.entity.job.main.IF_AttackableJob;
import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.E_Orientation;
import de.hetzge.sgame.misc.Util;
import de.hetzge.sgame.world.EntityGrid;
import de.hetzge.sgame.world.GridPosition;
import de.hetzge.sgame.world.IF_GridPosition;
import de.hetzge.sgame.world.World;

public class EntityFunction {

	public void destroyEntity(Entity entity) {
		Logger.info("destroy entity " + entity.getId());
		EntityDefinition definition = entity.getDefinition();
		entity.unsetPath();
		entity.setActivity(E_Activity.DESTROY);
		entity.setJob(new DestroyJob(entity));
		definition.getOnDestroy().accept(entity);
	}

	public void removeEntity(Entity entity) {
		Logger.info("remove entity " + entity.getId());
		EntityDefinition definition = entity.getDefinition();
		definition.getOnRemove().accept(entity);
		App.getGame().getEntityManager().remove(entity);
		App.getGame().getWorld().getEntityGrid().unset(entity);
		if (!definition.isMoveable()) {
			App.getGame().getWorld().getFixedCollisionGrid().unsetCollision(entity);
		}
	}

	public void createEntity(E_EntityType entityType, short x, short y, int entityId, byte playerId) {
		Entity entity = new Entity(entityId, playerId, entityType);
		entity.setGridX(x);
		entity.setGridY(y);
		newEntity(entity);
		entity.getDefinition().getOnCreate().accept(entity);
	}

	private void newEntity(Entity entity) {
		short gridX = entity.getGridX();
		short gridY = entity.getGridY();
		short width = entity.getWidth();
		short height = entity.getHeight();
		EntityDefinition definition = entity.getDefinition();
		boolean moveable = definition.isMoveable();

		App.getGame().getEntityManager().register(entity);
		App.getGame().getWorld().getEntityGrid().set(gridX, gridY, entity);

		if (!moveable) {
			App.getGame().getWorld().getFixedCollisionGrid().setCenteredCollision(gridX, gridY, width, height);
		}
	}

	public void convertEntity(Entity entity) {
		removeEntity(entity);
		createEntity(entity.getDefinition().getConvertsTo(), entity.getGridX(), entity.getGridY(), entity.getId(), entity.getOwner());
	}

	public void goAway(Entity entity) {
		goAway(entity, new LinkedList<>());
	}

	private void goAway(Entity entity, List<IF_GridPosition> blacklist) {
		World world = App.getGame().getWorld();
		EntityGrid entityGrid = world.getEntityGrid();

		IF_GridPosition gridPosition = entity.getRegisteredGridPosition();
		blacklist.add(gridPosition);

		for (E_Orientation orientation : E_Orientation.values) {
			GridPosition around = gridPosition.getAround(orientation);
			if (App.worldFunction.isEntityPossible(around, entity)) {
				gotoGridPositionAround(entity, orientation);
				entity.getJob().pauseLong();
				return;
			}
		}
		for (E_Orientation orientation : E_Orientation.values) {
			GridPosition around = gridPosition.getAround(orientation);
			if (blacklist.contains(around)) {
				continue;
			}
			Entity entityAround = entityGrid.get(around);
			if (entityAround != null && isGoAwayable(entityAround)) {
				goAway(entityAround, blacklist);
				return;
			}
		}
	}

	private boolean isGoAwayable(Entity entity) {
		return entity.getDefinition().isMoveable() && !entity.hasPath();
	}

	public boolean isOwnEntity(Entity entity) {
		return entity.getOwner() == App.getGame().getSelf().getPlayerId();
	}

	public boolean isDoorOpen(Entity entity) {
		return !App.getGame().getWorld().getFixedCollisionGrid().is(entity.getDoorGridPosition());
	}

	public Comparator<Entity> createDistanceComparator(Entity toEntity, boolean desc) {
		return (e1, e2) -> {
			return (int) (distance(e1, toEntity) - distance(e2, toEntity) * (desc ? -1 : 1));
		};
	}

	public float distance(Entity entityA, Entity entityB) {
		return Util.distance(entityA.getWorldX(), entityA.getWorldY(), entityB.getWorldX(), entityB.getWorldY());
	}

	public boolean isInRadius(Entity centerEntity, Entity checkEntity) {
		return distance(centerEntity, checkEntity) < centerEntity.getDefinition().getBorderRadius() * Constant.TILE_SIZE;
	}

	public boolean isInWorkDistance(Entity workingEntity, Entity otherEntity) {
		return distance(workingEntity, otherEntity) < workingEntity.getDefinition().getWorkDistance();
	}

	public boolean isInAggresionRadius(Entity aggresivEntity, Entity otherEntity) {
		return distance(aggresivEntity, otherEntity) < aggresivEntity.getDefinition().getAggresivRadius();
	}

	public boolean isAttackableEnemy(Entity attacker, Entity other) {
		byte owner = attacker.getOwner();
		byte otherOwner = other.getOwner();
		if (owner == otherOwner) {
			return false;
		}

		EntityDefinition otherDefinition = other.getDefinition();
		if (!otherDefinition.isAttackable()) {
			return false;
		}

		EntityJob otherJob = other.getJob();
		if (!(otherJob instanceof IF_AttackableJob)) {
			return false;
		}

		return true;
	}

	/**
	 * Set path to a direct neighbor of <code>entity</code> in the direction of
	 * the given <code>goal</code>.
	 */
	public void moveDirect(Entity entity, GridPosition goal) {
		E_Orientation orientation = E_Orientation.orientationTo(entity.getGridPosition(), goal);
		moveDirect(entity, orientation);
	}

	public void moveDirect(Entity entity, E_Orientation orientation) {
		GridPosition registeredGridPosition = entity.getRegisteredGridPosition();
		GridPosition around = registeredGridPosition.getAround(orientation);
		if (!entity.isPathGoal(around) && App.worldFunction.checkSpace(around, entity)) {
			entity.setPath(Arrays.asList(registeredGridPosition, around));
		}
	}

	public void gotoGridPositionAround(Entity entity, E_Orientation orientation) {
		GridPosition registeredGridPosition = entity.getRegisteredGridPosition();
		GridPosition around = registeredGridPosition.getAround(orientation);

		entity.setPath(Arrays.asList(registeredGridPosition, around));
	}

}
