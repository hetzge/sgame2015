package de.hetzge.sgame.gui;

import com.badlogic.gdx.scenes.scene2d.Stage;

import de.hetzge.sgame.App;

public class GuiStage extends Stage {

	// private final GuiContainer guiContainer;

	public GuiStage() {
		super(App.libGdxApplication.getGuiViewport());
		addActor(new GuiContainer());
	}

	public void resize(int width, int height) {
	}

}
