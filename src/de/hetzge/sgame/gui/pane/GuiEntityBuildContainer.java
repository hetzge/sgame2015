package de.hetzge.sgame.gui.pane;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Container;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.game.EntitySelections;

public class GuiEntityBuildContainer extends Container<Container<?>> {

	public GuiEntityBuildContainer() {
		App.getGame().getLocalGameState().getEntitySelections().getCallbacks().on(EntitySelections.E_Callback.CHANGED, this::onSelectionChanged);
	}

	private void onSelectionChanged(EntitySelections entitySelections) {
		clear();

		entitySelections.getFirstSelection().ifPresent(entity -> {
			if (App.entityFunction.isOwnEntity(entity)) {
				List<E_EntityType> canBuildEntityTypes = entity.getDefinition().getCanBuildEntityTypes();
				setActor(new GuiBuildContainer(canBuildEntityTypes));
			}
		});
	}

}
