package de.hetzge.sgame.gui.pane;

import com.badlogic.gdx.scenes.scene2d.ui.Container;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.EntitySelections;

public class GuiEntitySelectionPanel extends Container<GuiEntityDetailTable> {

	public GuiEntitySelectionPanel() {
		App.getGame().getLocalGameState().getEntitySelections().getCallbacks().on(EntitySelections.E_Callback.CHANGED, this::onSelectionChanged);
	}

	private void onSelectionChanged(EntitySelections entitySelections) {
		clear();
		entitySelections.getFirstSelection().ifPresent(it -> setActor(new GuiEntityDetailTable(it)));
	}

}
