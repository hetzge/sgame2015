package de.hetzge.sgame.gui.pane;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.graphic.GraphicKey;

public class GuiBuildContainer extends Container<Container<?>> {

	public GuiBuildContainer(List<E_EntityType> entityTypes) {

		Table table = new Table();
		table.setFillParent(true);

		int i = 0;
		for (E_EntityType entityType : entityTypes) {

			TextureRegion textureRegion = App.ressources.getGraphic(new GraphicKey(E_Activity.IDLE, entityType)).getKeyFrame(0f);
			TextureRegionDrawable drawable = new TextureRegionDrawable(textureRegion);
			Button imageButton = new Button(new Image(drawable), App.ressources.getSkin());

			imageButton.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					App.getGame().getLocalGameState().setEntityTypeToBuild(entityType);
					event.stop();
				}
			});

			table.add(imageButton).fill();

			if (i != 0 && i % 5 == 0) {
				table.row().fill();
			}

			i++;
		}

		Container<Table> container = new Container<>();
		container.setActor(table);
		container.setFillParent(true);
		setActor(container);
	}

}
