package de.hetzge.sgame.gui.pane;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.graphic.GraphicKey;
import de.hetzge.sgame.misc.Constant;

public class GuiEntityDetailTable extends Table {

	public GuiEntityDetailTable(Entity entity) {
		setSkin(App.ressources.getSkin());

		add(new Image(App.ressources.getGraphic(new GraphicKey(E_Activity.IDLE, entity.getEntityType())).getKeyFrame(0f)));
		add("");
		row();
		add("id");
		add(Integer.toString(entity.getId()));
		row();
		add("owner");
		add(Byte.toString(entity.getOwner()), Constant.DEFAULT_FONT_NAME, Constant.COLORS[entity.getOwner()]);
		row();
		add("type");
		add(entity.getEntityType().name());
	}

}
