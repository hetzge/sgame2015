package de.hetzge.sgame.gui.pane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.input.MouseEventPosition;

public class GuiMouseDebug extends VerticalGroup {

	private final Label worldLabel;
	private final Label gridLabel;
	private final Label screenLabel;

	public GuiMouseDebug() {
		addActor(this.gridLabel = new Label("", App.ressources.getSkin()));
		addActor(this.worldLabel = new Label("", App.ressources.getSkin()));
		addActor(this.screenLabel = new Label("", App.ressources.getSkin()));
	}

	@Override
	public void act(float delta) {
		MouseEventPosition mouseEventPosition = new MouseEventPosition(Gdx.input.getX(), Gdx.input.getY());
		this.gridLabel.setText(mouseEventPosition.gridToString());
		this.worldLabel.setText(mouseEventPosition.worldToString());
		this.screenLabel.setText(mouseEventPosition.screenToString());
		super.act(delta);
	}

}
