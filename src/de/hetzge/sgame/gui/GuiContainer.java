package de.hetzge.sgame.gui;

import java.util.Arrays;

import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.gui.pane.GuiBuildContainer;
import de.hetzge.sgame.gui.pane.GuiEntitySelectionPanel;

public class GuiContainer extends VerticalGroup {

	public GuiContainer() {
		setFillParent(true);
		
		addActor(new GuiBuildContainer(Arrays.asList(E_EntityType.values)));
		addActor(new GuiEntitySelectionPanel());
//		addActor(new GuiEntityBuildContainer());
//		addActor(new GuiMouseDebug());
	}

	public void resize(int width, int height) {
	}

}
