package de.hetzge.sgame.misc;

import java.io.Serializable;

import org.pmw.tinylog.Logger;

public class IdProvider implements Serializable {

	private String name;
	private int nextId;

	public IdProvider(String name, int nextId) {
		this.nextId = nextId;
		this.name = name;
	}

	public synchronized int next() {
		int next = this.nextId;
		this.nextId += 1;
		Logger.info(this.name + ": next id -> " + next);
		return next;
	}

}
