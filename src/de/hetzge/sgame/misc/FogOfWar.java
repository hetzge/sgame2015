package de.hetzge.sgame.misc;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

public class FogOfWar extends ApplicationAdapter {

	// TODO do i need depth ?
	private final Lazy<FrameBuffer> bufferLazy = new Lazy<>(() -> new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true));
	private final Color color;

	public FogOfWar() {
		this(Color.BLACK);
	}

	public FogOfWar(Color color) {
		this.color = color;
	}

	public void draw(Runnable renderRunnable) {
		this.bufferLazy.get().begin();

//		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
//		Gdx.gl.glEnable(GL20.GL_BLEND);

		Gdx.gl.glClearColor(this.color.r, this.color.g, this.color.b, this.color.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderRunnable.run();

		this.bufferLazy.get().end();
	}

	public void drawTo(SpriteBatch batch) {
		batch.enableBlending();
		batch.setBlendFunction(GL20.GL_ZERO, GL20.GL_SRC_COLOR);

		batch.begin();
		batch.draw(getTexture(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();

		batch.disableBlending();
	}

	@Override
	public void resize(int width, int height) {
		this.bufferLazy.reset();
	}

	@Override
	public void dispose() {
		this.bufferLazy.get().dispose();
	}

	private Texture getTexture() {
		return this.bufferLazy.get().getColorBufferTexture();
	}

}
