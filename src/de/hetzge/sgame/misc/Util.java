package de.hetzge.sgame.misc;

import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.hetzge.sgame.world.IF_GridPosition;

public final class Util {

	private Util() {
	}

	public static void sleep(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static int offset(int size) {
		return size % 2 == 0 ? size / 2 : (size - 1) / 2;
	}

	public static int index(short x, short y, short width) {
		return x + y * width;
	}

	public static int index(int x, int y, short width) {
		return x + y * width;
	}

	public static int index(int x, int y, int width) {
		return x + y * width;
	}

	public static int unIndexX(int index, int width) {
		return index % width;
	}

	public static int unIndexY(int index, int width) {
		return (int) Math.floor(index / width);
	}

	public static String toString(short x, short y) {
		return "(" + x + "|" + y + ")";
	}

	public static <T> T findOne(Iterator<T> iterator, Predicate<T> predicate) {
		while (iterator.hasNext()) {
			T next = iterator.next();
			if (predicate.test(next)) {
				return next;
			}
		}
		return null;
	}

	public static short worldToGrid(float world) {
		return (short) Math.floor(world / Constant.TILE_SIZE);
	}

	public static float gridToWorld(short grid) {
		return grid * Constant.TILE_SIZE;
	}

	public static Animation textureRegionToAnimation(TextureRegion textureRegion) {
		return new Animation(Constant.ANIMATION_FRAME_DURATION, new TextureRegion[] { textureRegion });
	}

	public static float distance(float x1, float y1, float x2, float y2) {
		float a = Math.abs(x1 - x2);
		float b = Math.abs(y1 - y2);
		return (float) Math.sqrt(a * a + b * b);
	}

	public static short[] toXArray(List<IF_GridPosition> gridPositions) {
		short[] result = new short[gridPositions.size()];
		for (int i = 0; i < gridPositions.size(); i++) {
			result[i] = gridPositions.get(i).getGridX();
		}
		return result;
	}

	public static short[] toYArray(List<IF_GridPosition> gridPositions) {
		short[] result = new short[gridPositions.size()];
		for (int i = 0; i < gridPositions.size(); i++) {
			result[i] = gridPositions.get(i).getGridY();
		}
		return result;
	}

}
