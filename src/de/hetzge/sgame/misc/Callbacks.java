package de.hetzge.sgame.misc;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class Callbacks<T> {

	private final T host;
	private final Map<Key, Set<Consumer<T>>> callbacks = new HashMap<>();

	public Callbacks(T host) {
		this.host = host;
	}

	public void on(Key key, Consumer<T> consumer) {
		Set<Consumer<T>> set = this.callbacks.getOrDefault(key, new LinkedHashSet<>());
		set.add(consumer);
		this.callbacks.put(key, set);
	}

	public void call(Key key) {
		Set<Consumer<T>> set = this.callbacks.getOrDefault(key, Collections.emptySet());
		for (Consumer<T> consumer : set) {
			consumer.accept(this.host);
		}
	}

	public interface Key {

	}

}
