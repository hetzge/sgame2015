package de.hetzge.sgame.entity.job.main;

import de.hetzge.sgame.App;
import de.hetzge.sgame.booking.Container;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;
import de.hetzge.sgame.entity.job.IF_RenderItemsJob;
import de.hetzge.sgame.item.E_Item;

public class BuildingLotJob extends EntityJob implements IF_RenderItemsJob {

	private final ConsumerJob consumerJob;

	public BuildingLotJob(Entity entity) {
		super(entity);
		this.consumerJob = new ConsumerJob(entity);
	}

	@Override
	protected void work() {
		this.consumerJob.doWork(this.entity);
		if (this.consumerJob.needs.isCompleteAvailable()) {
			App.entityFunction.convertEntity(this.entity);
		} else {
			pauseMedium();
		}
	}

	@Override
	public Container<E_Item> getRenderLeftContainer() {
		return this.consumerJob.needs;
	}

	@Override
	public Container<E_Item> getRenderRightContainer() {
		return null;
	}

}
