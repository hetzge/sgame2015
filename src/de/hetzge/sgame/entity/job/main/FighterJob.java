package de.hetzge.sgame.entity.job.main;

import java.util.Optional;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_Activity;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;
import de.hetzge.sgame.misc.E_Orientation;
import de.hetzge.sgame.world.EntityPartitionGrid;
import de.hetzge.sgame.world.GridPosition;

/**
 * TODO use direct walking instead of path
 */
public class FighterJob extends EntityJob implements IF_AttackableJob {

	private IF_AttackableJob attackableEnemy;
	private final IF_AttackableJob attackableJob;
	private int lps;

	public FighterJob(Entity entity) {
		super(entity);
		this.attackableJob = new AttackableJob(entity);
		this.lps = entity.getDefinition().getLps();
	}

	@Override
	protected void work() {
		if (this.entity.hasPath()) {
			GridPosition nextTile = this.entity.getRegisteredGridPosition().getAround(this.entity.getOrientation());
			Entity entityOnNext = App.getGame().getWorld().isOnGrid(nextTile) ? App.getGame().getWorld().getEntityGrid().get(nextTile) : null;
			if (entityOnNext != null && App.entityFunction.isAttackableEnemy(this.entity, entityOnNext)) {
				setAttackable((IF_AttackableJob) entityOnNext.getJob());
				fight();
			} else if (hasAttackable() && isEnemyInWorkRaidus() && this.entity.getGridPosition().distanceTo(this.entity.getPathGoalGridPosition()) > this.entity.getDefinition().getWorkDistance()) {
				fight();
			} else {
				this.entity.setActivity(E_Activity.WALKING);
			}
		} else {
			if (!hasAttackable()) {
				/*
				 * Search enemy
				 */
				EntityPartitionGrid entityPartitionGrid = App.getGame().getWorld().getEntityPartitionGrid();

				Optional<IF_AttackableJob> nearestAttackableOptional = entityPartitionGrid.getNear(this.entity).stream().filter(it -> App.entityFunction.isAttackableEnemy(this.entity, it)).filter(it -> App.entityFunction.isInAggresionRadius(this.entity, it)).sorted(App.entityFunction.createDistanceComparator(this.entity, false)).map(it -> (IF_AttackableJob) it.getJob()).findFirst();
				if (nearestAttackableOptional.isPresent()) {

					IF_AttackableJob nearestAttackable = nearestAttackableOptional.get();
					setAttackable(nearestAttackable);
				} else {
					pauseMedium();
				}
			} else if (isEnemyDestroyed()) {
				unsetAttackable();
				this.entity.setActivity(E_Activity.IDLE);
			} else if (isEnemyInWorkRaidus()) {
				fight();
			} else if (isEnemyInAgressionRadius()) {
				gotoEnemy();
			} else {
				this.entity.setActivity(E_Activity.IDLE);
				unsetAttackable();
				pauseSmall();
			}
		}
	}

	private boolean hasAttackable() {
		return this.attackableEnemy != null;
	}

	private boolean isEnemyDestroyed() {
		return this.attackableEnemy.getEntity().getActivity() == E_Activity.DESTROY;
	}

	private boolean isEnemyInAgressionRadius() {
		return App.entityFunction.isInAggresionRadius(this.entity, this.attackableEnemy.getEntity());
	}

	private boolean isEnemyInWorkRaidus() {
		return App.entityFunction.isInWorkDistance(this.entity, this.attackableEnemy.getEntity());
	}

	private void gotoEnemy() {
		App.entityFunction.moveDirect(this.entity, this.attackableEnemy.getEntity().getGridPosition());
	}

	private void fight() {
		Entity enemy = this.attackableEnemy.getEntity();
		GridPosition entityGridPosition = this.entity.getRegisteredGridPosition();
		GridPosition enemyGridPosition = enemy.getRegisteredGridPosition();
		E_Orientation orientationToEnemy = E_Orientation.orientationTo(entityGridPosition, enemyGridPosition);
		short workTimeInFrames = this.entity.getDefinition().getWorkTimeInFrames();

		this.entity.unsetPath();
		this.entity.setActivity(E_Activity.WORKING);
		this.entity.setOrientation(orientationToEnemy);
		this.attackableEnemy.attack(this, 1); // TODO

		pause(workTimeInFrames);
	}

	public void setAttackable(IF_AttackableJob enemy) {
		this.attackableEnemy = enemy;
	}

	public void unsetAttackable() {
		this.attackableEnemy = null;
	}

	@Override
	public void destroy() {
		unsetAttackable();
	}

	@Override
	public void attack(IF_AttackableJob attacker, int damageInLps) {
		this.lps -= damageInLps;

		if (this.attackableEnemy == null || App.entityFunction.distance(this.entity, attacker.getEntity()) < App.entityFunction.distance(this.entity, this.attackableEnemy.getEntity())) {
			setAttackable(attacker);
		}

		if (this.lps <= 0) {
			App.entityFunction.destroyEntity(this.entity);
		}

		this.attackableJob.attack(attacker, damageInLps);
	}

	@Override
	public void heal(int lps) {
		this.lps = Math.min(this.entity.getDefinition().getLps(), this.lps + lps);
	}

}
