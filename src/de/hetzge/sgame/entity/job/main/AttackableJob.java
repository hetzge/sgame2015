package de.hetzge.sgame.entity.job.main;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;

public class AttackableJob extends EntityJob implements IF_AttackableJob {

	private int lps;

	public AttackableJob(Entity entity) {
		super(entity);
		this.lps = entity.getDefinition().getLps();
	}

	@Override
	protected void work() {

	}

	@Override
	public void attack(IF_AttackableJob attacker, int damageInLps) {
		Logger.info(attacker.getEntity().getId() + " attacks " + getEntity().getId());
		
		this.lps -= damageInLps;

		if (this.lps <= 0) {
			App.entityFunction.destroyEntity(this.entity);
		}
	}

	@Override
	public void heal(int lps) {
		this.lps = Math.min(this.entity.getDefinition().getLps(), this.lps + lps);
	}

}
