package de.hetzge.sgame.entity.job.main;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;

public class DestroyJob extends EntityJob {

	private final int removeOnFrameId;

	public DestroyJob(Entity entity) {
		super(entity);
		int destroyTimeInFrames = entity.getDefinition().getDestroyTimeInFrames();
		this.removeOnFrameId = App.getGame().getTimeline().getNextFrameId(destroyTimeInFrames);
	}

	@Override
	protected void work() {
		Logger.info("do destroy " + getEntity().getId());
		if (App.getGame().getTimeline().isCurrentOrPast(this.removeOnFrameId)) {
			App.getGame().getEntityManager().registerRemove(this.entity);
		}
	}

}
