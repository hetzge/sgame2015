package de.hetzge.sgame.entity.job.main;

import de.hetzge.sgame.entity.Entity;

public interface IF_AttackableJob {

	void attack(IF_AttackableJob attacker, int damageInLps);

	void heal(int lps);

	Entity getEntity();
	
}
