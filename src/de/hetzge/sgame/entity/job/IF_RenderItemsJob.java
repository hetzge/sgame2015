package de.hetzge.sgame.entity.job;

import de.hetzge.sgame.booking.Container;
import de.hetzge.sgame.item.E_Item;

public interface IF_RenderItemsJob {

	/**
	 * Needs
	 */
	Container<E_Item> getRenderLeftContainer();

	/**
	 * Provides
	 */
	Container<E_Item> getRenderRightContainer();

}
