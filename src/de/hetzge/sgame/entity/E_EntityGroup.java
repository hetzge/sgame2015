package de.hetzge.sgame.entity;

/**
 * Categorize {@link E_EntityType}s in different functional groups.
 */
public enum E_EntityGroup {

	UNDEFINED, UNIT, BUILDING, BUILDING_LOT, ENVIRONMENT;

}
