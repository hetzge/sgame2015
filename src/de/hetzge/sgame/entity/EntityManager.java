package de.hetzge.sgame.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import de.hetzge.sgame.misc.Constant;
import de.hetzge.sgame.misc.IdProvider;

public class EntityManager implements Serializable {

	private final IdProvider idProvider = new IdProvider("game", 0);

	/**
	 * TODO replace with int map
	 */
	private final Map<Integer, Entity> entitiesById = new HashMap<>();

	private final Set<Entity> entities = new TreeSet<>(new Entity.IdComparator());
	private final Set<Entity> removeEntities = new TreeSet<>(new Entity.IdComparator());

	public void register(Entity entity) {
		int entityId = entity.getId();
		if (this.entitiesById.containsKey(entityId)) {
			throw new IllegalStateException("Try to register entity with already registered id " + entityId);
		}
		this.entitiesById.put(entity.getId(), entity);
		this.entities.add(entity);
	}

	public void registerRemove(Entity entity) {
		this.removeEntities.add(entity);
	}

	public void remove(Entity entity) {
		int entityId = entity.getId();
		this.entitiesById.remove(entityId);
		this.entities.remove(entity);
	}

	public boolean doExist(int entityId) {
		return this.entitiesById.containsKey(entityId);
	}

	public Entity get(int entityId) {
		Entity entity = this.entitiesById.get(entityId);
		if (entity == null) {
			if (entityId == Constant.NO_ENTITY_ID) {
				return null;
			} else {
				throw new IllegalStateException("Try to access non existing entity " + entityId);
			}
		} else {
			return entity;
		}
	}

	public List<Entity> getUnsecure(List<Integer> entityIds) {
		List<Entity> result = new ArrayList<>(entityIds.size());
		for (Integer entityId : entityIds) {
			if (doExist(entityId)) {
				Entity entity = get(entityId);
				result.add(entity);
			}
		}
		return result;
	}

	public List<Entity> get(List<Integer> entityIds) {
		List<Entity> result = new ArrayList<>(entityIds.size());
		for (Integer entityId : entityIds) {
			Entity entity = get(entityId);
			result.add(entity);
		}
		return result;
	}

	public List<Entity> flushRemoveEntities() {
		if (this.removeEntities.isEmpty()) {
			return Collections.emptyList();
		} else {
			List<Entity> result = new ArrayList<>(this.removeEntities);
			this.removeEntities.clear();
			return result;
		}
	}

	public Set<Entity> getEntities() {
		return this.entities;
	}

	public IdProvider getIdProvider() {
		return this.idProvider;
	}

}
