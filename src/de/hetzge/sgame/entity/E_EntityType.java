package de.hetzge.sgame.entity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.hetzge.sgame.entity.definition.EntityDefinition;
import de.hetzge.sgame.item.E_Item;

public enum E_EntityType {

	WORKER_LUMBERJACK(new EntityDefinition.Miner(E_Item.WOOD)), WORKER_MASON(new EntityDefinition.Miner(E_Item.STONE)),

	TREE(new EntityDefinition.Provider(E_Item.WOOD)), CAIRN(new EntityDefinition.Provider(E_Item.STONE)),

	CARRIER(new EntityDefinition.Carrier()),

	BUILDING_LUMBERJACK(new EntityDefinition.Workstation(E_Item.WOOD)), BUILDING_QUARRY(new EntityDefinition.Workstation(E_Item.STONE)),

	BUILDING_FACTORY(new EntityDefinition.Factory()),

	BUILDING_LUMBERJACK_LOT(new EntityDefinition.BuildingLot(BUILDING_LUMBERJACK, new HashMap<E_Item, Integer>() {
		{
			put(E_Item.WOOD, 2);
		}
	})), BUILDING_QUARRY_LOT(new EntityDefinition.BuildingLot(BUILDING_QUARRY, new HashMap<E_Item, Integer>() {
		{
			put(E_Item.WOOD, 2);
		}
	})), BUILDING_FACTORY_LOT(new EntityDefinition.BuildingLot(BUILDING_FACTORY, new HashMap<E_Item, Integer>() {
		{
			put(E_Item.WOOD, 2);
		}
	})), LANDMARK(new EntityDefinition.Landmark()), LANDMARK_LOT(new EntityDefinition.BuildingLot(LANDMARK, new HashMap<E_Item, Integer>() {
		{
			put(E_Item.STONE, 2);
		}
	})), FIGHTER(new EntityDefinition.Fighter());

	public static final Set<E_EntityType> BUILDINGS;
	public static final Set<E_EntityType> BUILDING_LOTS;
	public static final Set<E_EntityType> UNITS;
	public static final Set<E_EntityType> ENVIRONMENTS;

	static {
		Function<E_EntityGroup, Set<E_EntityType>> filter = group -> Arrays.asList(E_EntityType.values()).stream().filter(it -> it.getEntityDefinition().getGroup() == group).collect(Collectors.toSet());

		BUILDINGS = filter.apply(E_EntityGroup.BUILDING);
		BUILDING_LOTS = filter.apply(E_EntityGroup.BUILDING_LOT);
		UNITS = filter.apply(E_EntityGroup.UNIT);
		ENVIRONMENTS = filter.apply(E_EntityGroup.ENVIRONMENT);
	}

	public static final E_EntityType[] values = values();

	private final EntityDefinition entityDefinition;

	private E_EntityType(EntityDefinition entityDefinition) {
		this.entityDefinition = entityDefinition;
	}

	public EntityDefinition getEntityDefinition() {
		return this.entityDefinition;
	}

	public boolean isEnvironment() {
		return ENVIRONMENTS.contains(this);
	}

	public boolean isBuilding() {
		return BUILDINGS.contains(this);
	}

	public boolean isBuildingLot() {
		return BUILDING_LOTS.contains(this);
	}

	public boolean isUnit() {
		return UNITS.contains(this);
	}

}
