package de.hetzge.sgame.entity.definition;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import de.hetzge.sgame.App;
import de.hetzge.sgame.booking.Container;
import de.hetzge.sgame.booking.Ingredient;
import de.hetzge.sgame.booking.Receipt;
import de.hetzge.sgame.entity.E_EntityGroup;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.entity.job.EntityJob;
import de.hetzge.sgame.entity.job.main.AttackableJob;
import de.hetzge.sgame.entity.job.main.BuildingLotJob;
import de.hetzge.sgame.entity.job.main.CarrierJob;
import de.hetzge.sgame.entity.job.main.FactoryJob;
import de.hetzge.sgame.entity.job.main.FighterJob;
import de.hetzge.sgame.entity.job.main.MineProviderJob;
import de.hetzge.sgame.entity.job.main.MinerJob;
import de.hetzge.sgame.entity.job.main.WorkstationJob;
import de.hetzge.sgame.item.E_Item;
import de.hetzge.sgame.item.GridEntityContainer;
import de.hetzge.sgame.misc.Constant;

public abstract class EntityDefinition {

	/**
	 * Life points
	 */
	protected final int lps = 10;

	/**
	 * Function category of the by this definition defined entity.
	 */
	protected final E_EntityGroup group;

	/**
	 * A {link Supplier} which provides a list with types of entities which can
	 * be build by the defined entity. This need to be a lazy supplier because
	 * while definition {@link E_EntityType} is not initialized.
	 */
	protected Supplier<List<E_EntityType>> canBuildEntityTypesSupplier = () -> Collections.emptyList();

	/**
	 * Can the player select the entity.
	 */
	protected boolean selectable = false;

	/**
	 * Can the player control the selected entity manually, like going to a
	 * pointed place.
	 */
	protected boolean controllable = false;

	/**
	 * Is it a fixed or flexible entity. Flexible entities can move around.
	 * Fixed ones are bound to the tile grid.
	 */
	protected boolean moveable = false;

	/**
	 * Controls if the entity can be attacked by a other entity.
	 */
	protected boolean attackable = false;

	/**
	 * Controls if borders are relevant for a entity. If false the entity can
	 * cross borders.
	 */
	protected boolean bordered = true;

	/**
	 * The width of the entity in tiles.
	 */
	protected short width = 1;

	/**
	 * The height of the entity in tiles.
	 */
	protected short height = 1;

	protected int energie = 1000;
	protected int buildTimeInFrames = 3000; // TODO
	protected float speed = 30f;

	/**
	 * Towers and landmarks define with this the radius of the border around it.
	 */
	protected int borderRadius = 1;

	/**
	 * Radius soldiers react aggressively inside.
	 */
	protected float aggresivRadius = Constant.TILE_SIZE * 5;

	/**
	 * Distance to other entities a worker can work or soldiers can fight.
	 */
	protected float workDistance = Constant.TILE_SIZE * 2;

	protected short workTimeInFrames = 30; // TODO integrate in jobs
	protected E_Item mineItem = null;

	/**
	 * The x offset relative to the grid position of the entity.
	 */
	protected short doorOffsetX = 0;

	/**
	 * The y offset relative to the grid position of the entity.
	 */
	protected short doorOffsetY = 1;
	protected int updateEveryFrames = 100; // TODO
	protected int destroyTimeInFrames = 3;

	/**
	 * Describes the {@link Container} of the provides which could be extracted
	 * once from the entity. The amount describes the available amount.
	 * 
	 * @see EntityDefinition#createDefaultMineProvideContainer(Entity)
	 */
	protected Map<E_Item, Integer> mineProvides = new HashMap<>();

	/**
	 * Describes the {@link Container} of the provides of the entity. Provides
	 * are the {@link E_Item}s which can be taken from the entity. Typically the
	 * provides are empty and the amount describes the maximum amount.
	 * 
	 * @see EntityDefinition#createDefaultMineProvideContainer(Entity)
	 */
	protected Map<E_Item, Integer> provides = new HashMap<>();

	/**
	 * Describes the {@link Container} of the needs of the entity. Needs are the
	 * {@link E_Item}s which the entity requests to bring to it.
	 * 
	 * @see EntityDefinition#createDefaultNeedContainer(Entity)
	 */
	protected Map<E_Item, Integer> needs = new HashMap<>();

	/**
	 * A function which will be called to create the job object of a entity.
	 */
	protected Function<Entity, EntityJob> jobSupplier = entity -> Constant.NO_JOB;

	/**
	 * A list of receipts which a entity could use to produce other items.
	 */
	protected List<Receipt<E_Item>> receipts = Collections.emptyList();

	/**
	 * A {@link E_EntityType} to which a entity could possibly envolve. As
	 * example a building lot can envolve to the finished building.
	 */
	protected E_EntityType convertsTo = null;

	/**
	 * A function which will be called after entity creation.
	 */
	protected Consumer<Entity> onCreate = entity -> {
	};

	/**
	 * A function which will be called before the entity is removed.
	 */
	protected Consumer<Entity> onRemove = entity -> {
	};

	/**
	 * A function which will be called before the entity is destroyed.
	 */
	protected Consumer<Entity> onDestroy = entity -> {
	};

	public int getLps() {
		return this.lps;
	}

	public EntityDefinition(E_EntityGroup group) {
		this.group = group;
	}

	public E_EntityGroup getGroup() {
		return this.group;
	}

	public List<E_EntityType> getCanBuildEntityTypes() {
		return this.canBuildEntityTypesSupplier.get();
	}

	public boolean isSelectable() {
		return this.selectable;
	}

	public boolean isMoveable() {
		return this.moveable;
	}

	public boolean isAttackable() {
		return this.attackable;
	}

	public boolean isControllable() {
		return this.controllable;
	}

	public short getWidth() {
		return this.width;
	}

	public short getHeight() {
		return this.height;
	}

	public int getEnergie() {
		return this.energie;
	}

	public int getBuildTimeInFrames() {
		return this.buildTimeInFrames;
	}

	public float getSpeed() {
		return this.speed;
	}

	public int getBorderRadius() {
		return this.borderRadius;
	}

	public float getAggresivRadius() {
		return this.aggresivRadius;
	}

	public float getWorkDistance() {
		return this.workDistance;
	}

	public short getWorkTimeInFrames() {
		return this.workTimeInFrames;
	}

	public E_Item getMineItem() {
		return this.mineItem;
	}

	public short getDoorOffsetX() {
		return this.doorOffsetX;
	}

	public short getDoorOffsetY() {
		return this.doorOffsetY;
	}

	public int getDestroyTimeInFrames() {
		return this.destroyTimeInFrames;
	}

	public List<Receipt<E_Item>> getReceipts() {
		return this.receipts;
	}

	public boolean doProvide(E_Item item) {
		return this.provides.containsKey(item);
	}

	public boolean doNeeds(E_Item item) {
		return this.needs.containsKey(item);
	}

	public EntityJob createJob(Entity entity) {
		return this.jobSupplier.apply(entity);
	}

	public boolean isBordered() {
		return this.bordered;
	}

	public E_EntityType getConvertsTo() {
		return this.convertsTo;
	}

	public Consumer<Entity> getOnCreate() {
		return this.onCreate;
	}

	public Consumer<Entity> getOnRemove() {
		return this.onRemove;
	}

	public Consumer<Entity> getOnDestroy() {
		return this.onDestroy;
	}

	public Container<E_Item> createDefaultMineProvideContainer(Entity entity) {
		Container<E_Item> container = new GridEntityContainer(entity);
		for (Entry<E_Item, Integer> entry : this.mineProvides.entrySet()) {
			E_Item item = entry.getKey();
			Integer value = entry.getValue();
			container.set(item, value);
		}
		return container;
	}

	public Container<E_Item> createDefaultProvideContainer(Entity entity) {
		Container<E_Item> container = new GridEntityContainer(entity);
		for (Entry<E_Item, Integer> entry : this.provides.entrySet()) {
			E_Item item = entry.getKey();
			Integer value = entry.getValue();
			if (value == 0) {
				container.set(item, 0, Constant.DEFAULT_MAX_ITEMS);
			} else {
				container.set(item, 0, value);
			}
		}
		return container;
	}

	public Container<E_Item> createDefaultNeedContainer(Entity entity) {
		Container<E_Item> container = new GridEntityContainer(entity);
		for (Entry<E_Item, Integer> entry : this.needs.entrySet()) {
			E_Item item = entry.getKey();
			Integer value = entry.getValue();
			container.set(item, 0, value);
		}
		return container;
	}

	public static class Dummy extends EntityDefinition {
		public Dummy() {
			super(E_EntityGroup.UNIT);
			this.moveable = true;
		}
	}

	public static class Miner extends EntityDefinition {
		public Miner(E_Item item) {
			super(E_EntityGroup.UNIT);
			this.moveable = true;
			this.jobSupplier = entity -> new MinerJob(entity);
			this.mineItem = item;
		}
	}

	public static class Provider extends EntityDefinition {
		public Provider(E_Item item) {
			super(E_EntityGroup.ENVIRONMENT);
			Map<E_Item, Integer> provides = new HashMap<>();
			provides.put(item, 3);
			this.mineProvides = provides;
			this.jobSupplier = entity -> new MineProviderJob(entity);
		}
	}

	public static class Workstation extends EntityDefinition {
		public Workstation(E_Item item) {
			super(E_EntityGroup.BUILDING);
			Map<E_Item, Integer> provides = new HashMap<>();
			provides.put(item, Constant.DEFAULT_MAX_ITEMS);
			this.provides = provides;
			this.mineItem = item;
			this.jobSupplier = entity -> new WorkstationJob(entity);
			this.width = 2;
			this.height = 2;
			this.doorOffsetX = 0;
			this.doorOffsetY = 1;
			this.canBuildEntityTypesSupplier = () -> Arrays.asList(E_EntityType.WORKER_LUMBERJACK);
		}
	}

	public static class Carrier extends EntityDefinition {
		public Carrier() {
			super(E_EntityGroup.UNIT);
			this.moveable = true;
			this.jobSupplier = entity -> new CarrierJob(entity);
		}
	}

	public static class Factory extends EntityDefinition {
		public Factory() {
			super(E_EntityGroup.BUILDING);
			HashMap<E_Item, Integer> needs = new HashMap<>();
			needs.put(E_Item.WOOD, Constant.DEFAULT_MAX_ITEMS);
			needs.put(E_Item.STONE, Constant.DEFAULT_MAX_ITEMS);
			HashMap<E_Item, Integer> provides = new HashMap<>();
			provides.put(E_Item.FISCH, Constant.DEFAULT_MAX_ITEMS);
			this.needs = needs;
			this.provides = provides;
			this.jobSupplier = entity -> new FactoryJob(entity);
			this.receipts = Arrays.asList(new Receipt(Arrays.asList(new Ingredient<>(E_Item.WOOD, 2), new Ingredient<>(E_Item.STONE, 2)), E_Item.FISCH));
		}
	}

	public static class BuildingLot extends EntityDefinition {
		public BuildingLot(E_EntityType entityType, HashMap<E_Item, Integer> needs) {
			super(E_EntityGroup.BUILDING_LOT);
			EntityDefinition entityDefinition = entityType.getEntityDefinition();
			this.width = entityDefinition.getWidth();
			this.height = entityDefinition.getHeight();
			this.doorOffsetX = entityDefinition.getDoorOffsetX();
			this.doorOffsetY = entityDefinition.getDoorOffsetY();
			this.convertsTo = entityType;
			this.needs = needs;
			this.jobSupplier = entity -> new BuildingLotJob(entity);
		}
	}

	public static class Landmark extends EntityDefinition {
		public Landmark() {
			super(E_EntityGroup.BUILDING);
			this.width = 1;
			this.height = 1;
			this.borderRadius = 10;
			this.jobSupplier = entity -> new AttackableJob(entity);
			this.onCreate = App.worldFunction::conquerTerritory;
			this.onRemove = App.worldFunction::loseTerritory;
			this.attackable = true;
		}
	}

	public static class Fighter extends EntityDefinition {
		public Fighter() {
			super(E_EntityGroup.UNIT);
			this.borderRadius = 2;
			this.moveable = true;
			this.jobSupplier = entity -> new FighterJob(entity);
			this.selectable = true;
			this.controllable = true;
			this.attackable = true;
			this.bordered = false;
		}
	}

}
