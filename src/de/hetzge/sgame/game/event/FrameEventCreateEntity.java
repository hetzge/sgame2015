package de.hetzge.sgame.game.event;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.entity.definition.EntityDefinition;
import de.hetzge.sgame.frame.FrameEvent;
import de.hetzge.sgame.world.IF_GridPosition;

public class FrameEventCreateEntity extends FrameEvent {

	private final E_EntityType entityType;
	private final short x;
	private final short y;
	private final int entityId;
	private final byte playerId;

	public FrameEventCreateEntity(int frameId, E_EntityType entityType, short x, short y, int entityId, byte playerId) {
		super(frameId);
		this.entityType = entityType;
		this.x = x;
		this.y = y;
		this.entityId = entityId;
		this.playerId = playerId;
	}

	@Override
	public void execute() {
		execute(this.x, this.y);

	}

	public void execute(short x, short y) {
		boolean hasSpace = App.worldFunction.checkSpaceForEntity(this.playerId, this.entityType, x, y);
		if (hasSpace) {
			App.entityFunction.createEntity(this.entityType, x, y, this.entityId, this.playerId);
		} else {
			EntityDefinition definition = this.entityType.getEntityDefinition();
			boolean isMoveable = definition.isMoveable();
			if (isMoveable) {
				IF_GridPosition freeGridPosition = App.worldFunction.findEmptyGridPositionAround(this.entityType, x, y);
				if (freeGridPosition != null && App.worldFunction.checkSpace(x, y, this.playerId, this.entityType)) {
					short newX = freeGridPosition.getGridX();
					short newY = freeGridPosition.getGridY();

					execute(newX, newY);
				} else {
					Logger.info("Didn't created entity from " + toString() + " caused by no space around found.");
				}
			} else {
				Logger.info("Didn't created entity from " + toString() + " caused by no space.");
			}
		}
	}

	@Override
	public String toString() {
		return "FrameEventCreateEntity [entityType=" + this.entityType + ", x=" + this.x + ", y=" + this.y + ", entityId=" + this.entityId + ", playerId=" + this.playerId + "]";
	}

}
