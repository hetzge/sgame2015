package de.hetzge.sgame.game.event.request;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.game.event.FrameEventCreateEntity;
import de.hetzge.sgame.game.event.IF_Event;
import de.hetzge.sgame.misc.Util;

public class EventRequestCreateEntity implements IF_Event {

	private final short x;
	private final short y;
	private final E_EntityType entityType;
	private final byte playerId;

	public EventRequestCreateEntity(short x, short y, E_EntityType entityType, byte playerId) {
		this.x = x;
		this.y = y;
		this.entityType = entityType;
		this.playerId = playerId;
	}

	@Override
	public void execute() {
		boolean hasSpace = App.worldFunction.checkSpaceForEntity(this.playerId, this.entityType, this.x, this.y);
		if (hasSpace) {
			int nextEntityId = App.getGame().getEntityManager().getIdProvider().next();
			FrameEventCreateEntity frameEventCreateEntity = new FrameEventCreateEntity(
					App.getGame().getTimeline().getDefaultNextFrameId(), this.entityType, this.x, this.y, nextEntityId,
					this.playerId);
			App.network.sendAndSelf(frameEventCreateEntity);
		} else {
			Logger.info("There is no space for " + this.entityType + " at " + Util.toString(this.x, this.y));
		}
	}

}
