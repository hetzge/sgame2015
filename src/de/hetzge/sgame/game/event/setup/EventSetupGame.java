package de.hetzge.sgame.game.event.setup;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.EntityManager;
import de.hetzge.sgame.game.event.IF_Event;
import de.hetzge.sgame.setting.Player;
import de.hetzge.sgame.world.World;

public class EventSetupGame implements IF_Event {

	private final Player playerSettings;
	private final EntityManager entityManager;
	private final World world;

	public EventSetupGame(Player playerSettings, EntityManager entityManager, World world) {
		this.playerSettings = playerSettings;
		this.entityManager = entityManager;
		this.world = world;
	}

	@Override
	public void execute() {
		App.getGame().setSelf(this.playerSettings);
		App.getGame().setWorld(this.world);
		App.getGame().setEntityManager(this.entityManager);
	}

}
