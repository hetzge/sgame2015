package de.hetzge.sgame.game.event;

import org.nustaq.net.TCPObjectSocket;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.event.setup.EventSetupGame;
import de.hetzge.sgame.network.NetworkFunction;
import de.hetzge.sgame.setting.Player;

public class EventPlayerHandshake implements IF_ConnectionEvent {

	private final String playerName;

	public EventPlayerHandshake(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public void execute(TCPObjectSocket tcpObjectSocket) {
		Player playerSettings = new Player(App.getGame().getPlayers().nextPlayerId(), this.playerName);
		NetworkFunction.send(tcpObjectSocket, new EventSetupGame(playerSettings, App.getGame().getEntityManager(), App.getGame().getWorld()));
	}

}
