package de.hetzge.sgame.game;

import org.pmw.tinylog.Logger;

import de.hetzge.sgame.entity.EntityManager;
import de.hetzge.sgame.frame.Timeline;
import de.hetzge.sgame.setting.Player;
import de.hetzge.sgame.world.World;

public class Game {

	private boolean started = false;
	private long seed = 0;
	private World world = null;
	private Players players = null;
	private Player self = null;
	private EntityManager entityManager = new EntityManager();
	private LocalGameState localGameState = new LocalGameState();
	private Timeline timeline;

	public World getWorld() {
		return this.world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public long getSeed() {
		return this.seed;
	}

	public void setSeed(long seed) {
		this.seed = seed;
	}

	public Players getPlayers() {
		return this.players;
	}

	public void setPlayers(Players players) {
		this.players = players;
	}

	public Player getSelf() {
		return this.self;
	}

	public void setSelf(Player self) {
		Logger.info("set self with player id " + self.getPlayerId());
		this.self = self;
	}

	public boolean isStarted() {
		return this.started;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void start() {
		this.started = true;
		if (!isReadyToStart()) {
			throw new IllegalStateException("Try to start non ready game.");
		}
		this.timeline.startFrameTimer();
	}

	public boolean isComplete() {
		return this.world != null && this.world.getEntityGrid() != null && this.self != null && this.players != null;
	}

	public boolean isReadyToStart() {
		return isComplete() && isStarted();
	}

	public LocalGameState getLocalGameState() {
		return this.localGameState;
	}

	public Timeline getTimeline() {
		return this.timeline;
	}

	public void setTimeline(Timeline timeline) {
		this.timeline = timeline;
	}

}
