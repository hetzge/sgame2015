package de.hetzge.sgame.game;

import org.pmw.tinylog.Configurator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.scene.E_GameScene;

public class LibGdxApplication extends com.badlogic.gdx.Game {

	private OrthographicCamera camera;
	private OrthographicCamera guiCamera;
	private SpriteBatch spriteBatch;
	private ShapeRenderer shapeRenderer;
	private Viewport viewport;
	private Viewport guiViewport;
	private FrameBuffer frameBuffer;
	private TextureRegion frameBufferRegion;

	public static void main(String[] args) {
		LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
		configuration.title = "Game";
		configuration.width = App.settings.getSystemSettings().getResolutionX();
		configuration.height = App.settings.getSystemSettings().getResolutionY();
		configuration.fullscreen = App.settings.getSystemSettings().isFullscreen();
		ShaderProgram.pedantic = false;

		Configurator.defaultConfig().formatPattern("{date:HH:mm:SSS}|{level}|{class_name}:{method}:{line}|> {message}").activate();

		new LwjglApplication(App.libGdxApplication, configuration);
	}

	public void switchGameScene(E_GameScene gameScene) {
		setScreen(gameScene.getScreen());
	}

	@Override
	public void create() {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		this.camera = new OrthographicCamera(width, height);
		this.guiCamera = new OrthographicCamera(width, height);
		this.spriteBatch = new SpriteBatch();
		this.shapeRenderer = new ShapeRenderer();
		createFrameBuffer(width, height);
		this.shapeRenderer.setAutoShapeType(true);
		this.viewport = new ScreenViewport(this.camera);
		this.guiViewport = new ScreenViewport(this.guiCamera);
		switchGameScene(E_GameScene.LOAD);
	}

	@Override
	public void render() {
		this.camera.update();
		this.spriteBatch.setProjectionMatrix(this.camera.combined);
		this.shapeRenderer.setProjectionMatrix(this.camera.combined);
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		this.viewport.update(width, height);
		this.guiViewport.update(width, height, true);
		createFrameBuffer(width, height);
		getScreen().resize(width, height);
	}

	private void createFrameBuffer(int width, int height) {
		this.frameBuffer = new FrameBuffer(Format.RGBA8888, width, height, false);
		this.frameBufferRegion = new TextureRegion(this.frameBuffer.getColorBufferTexture());
	}

	public OrthographicCamera getCamera() {
		return this.camera;
	}

	public OrthographicCamera getGuiCamera() {
		return this.guiCamera;
	}

	public SpriteBatch getSpriteBatch() {
		return this.spriteBatch;
	}

	public ShapeRenderer getShapeRenderer() {
		return this.shapeRenderer;
	}

	public FrameBuffer getFrameBuffer() {
		return this.frameBuffer;
	}

	public TextureRegion getFrameBufferRegion() {
		return this.frameBufferRegion;
	}

	public Viewport getViewport() {
		return this.viewport;
	}

	public Viewport getGuiViewport() {
		return this.guiViewport;
	}

	public Vector2 unproject(int x, int y) {
		Vector3 project = this.camera.unproject(new Vector3(x, y, 0));
		return new Vector2(project.x, -project.y);
	}

}
