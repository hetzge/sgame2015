package de.hetzge.sgame.game;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.misc.Callbacks;
import de.hetzge.sgame.misc.Callbacks.Key;

public class EntitySelections {

	private final Callbacks<EntitySelections> callbacks = new Callbacks<>(this);
	private final LinkedHashSet<Entity> selections = new LinkedHashSet<>();

	public Callbacks<EntitySelections> getCallbacks() {
		return this.callbacks;
	}

	public boolean hasSelection() {
		return !this.selections.isEmpty();
	}

	public void clearSelection() {
		this.selections.clear();
	}

	public Set<Entity> getSelections() {
		return new LinkedHashSet<>(this.selections);
	}

	public Optional<Entity> getFirstSelection() {
		if (!this.selections.isEmpty()) {
			return Optional.of(this.selections.iterator().next());
		} else {
			return Optional.empty();
		}
	}

	public List<Entity> getSelection(Predicate<Entity> predicate) {
		return this.selections.stream().filter(predicate).collect(Collectors.toList());
	}

	public void addSelection(Entity entity) {
		this.selections.add(entity);
		fireChanged();
	}

	public void addSelection(List<Entity> entities) {
		this.selections.addAll(entities);
		fireChanged();
	}

	public void setSelection(List<Entity> entities) {
		clearSelection();
		this.selections.addAll(entities);
		fireChanged();
	}

	public int getSelectionSize() {
		return this.selections.size();
	}

	private void fireChanged() {
		System.out.println("CHANGED ..........");
		this.callbacks.call(E_Callback.CHANGED);
	}

	public enum E_Callback implements Key {
		CHANGED;
	}
}
