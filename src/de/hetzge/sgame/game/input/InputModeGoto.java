package de.hetzge.sgame.game.input;

import java.util.List;
import java.util.stream.Collectors;

import com.badlogic.gdx.Input;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.Entity;
import de.hetzge.sgame.game.EntitySelections;
import de.hetzge.sgame.game.LocalGameState;
import de.hetzge.sgame.game.event.request.EventRequestGoto;

public class InputModeGoto implements IF_InputMode {

	@Override
	public void onMouseDown(int button, MouseEventPosition downPosition) {
	}

	@Override
	public void onMouseUp(int button, MouseEventPosition downPosition, MouseEventPosition upPosition) {
		if (button == Input.Buttons.RIGHT) {
			LocalGameState localGameState = App.getGame().getLocalGameState();
			EntitySelections entitySelections = localGameState.getEntitySelections();
			boolean hasSelection = entitySelections.hasSelection();
			if (hasSelection) {
				List<Entity> selectionEntities = entitySelections.getSelection(entity -> entity.getDefinition().isControllable());
				List<Integer> selectionEntityIds = selectionEntities.stream().map(Entity::getId).collect(Collectors.toList());
				short gridX = upPosition.getGridX();
				short gridY = upPosition.getGridY();
				EventRequestGoto eventRequestGoto = new EventRequestGoto(selectionEntityIds, gridX, gridY);
				App.network.sendOrSelf(eventRequestGoto);
			}
		}
	}

}
