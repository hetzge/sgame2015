package de.hetzge.sgame.game.input;

import org.pmw.tinylog.Logger;

import com.badlogic.gdx.Input.Buttons;

import de.hetzge.sgame.App;
import de.hetzge.sgame.entity.E_EntityType;
import de.hetzge.sgame.game.Game;
import de.hetzge.sgame.game.LocalGameState;
import de.hetzge.sgame.game.event.request.EventRequestCreateEntity;

public class InputModeBuild implements IF_InputMode {

	@Override
	public void onMouseDown(int button, MouseEventPosition downPosition) {
	}

	@Override
	public void onMouseUp(int button, MouseEventPosition downPosition, MouseEventPosition upPosition) {

		if (!downPosition.equals(upPosition)) {
			return;
		}

		Game game = App.getGame();
		LocalGameState localGameState = game.getLocalGameState();
		E_EntityType entityTypeToBuild = localGameState.getEntityTypeToBuild();

		if (entityTypeToBuild == null) {
			return;
		}

		if (button == Buttons.RIGHT) {
			localGameState.setEntityTypeToBuild(null);
			return;
		}

		Logger.info("Try to build " + entityTypeToBuild + " at " + upPosition.gridToString());

		byte playerId = game.getSelf().getPlayerId();
		short gridX = upPosition.getGridX();
		short gridY = upPosition.getGridY();
		boolean checkSpaceForEntity = App.worldFunction.checkSpaceForEntity(playerId, entityTypeToBuild, gridX, gridY);

		if (!checkSpaceForEntity) {
			Logger.info("Can't build here because no space");
			return;
		}

		EventRequestCreateEntity eventRequestCreateEntity = new EventRequestCreateEntity(gridX, gridY, entityTypeToBuild, playerId);
		App.network.sendOrSelf(eventRequestCreateEntity);

		localGameState.unsetEntityTypeToBuild();
	}

}
