package de.hetzge.sgame.game.scene;

import java.io.File;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

import de.hetzge.sgame.App;
import de.hetzge.sgame.game.ConnectGameGui;
import de.hetzge.sgame.game.Game;
import de.hetzge.sgame.game.ImportExportException;
import de.hetzge.sgame.network.E_NetworkRole;

public class ConnectGameScene implements Screen {

	private ConnectGameGui connectGameGui;

	public ConnectGameScene() {
	}

	@Override
	public void show() {
		this.connectGameGui = new ConnectGameGui();
		this.connectGameGui.show();
		Gdx.input.setInputProcessor(this.connectGameGui);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		this.connectGameGui.act();
		this.connectGameGui.draw();

		if (!App.network.isConnected()) {
			newGame();
		}
		if (App.getGame().isReadyToStart()) {
			startGame();
		} else if (isHost() && App.getGame().isComplete()) {
			this.connectGameGui.showStartButton();
		}
	}

	private void startGame() {
		App.libGdxApplication.switchGameScene(E_GameScene.INGAME);
	}

	private void newGame() {
		App.setGame(new Game());
		if (isHost()) {
			try {
				App.function.initGame(App.importer.importGameFormatFromTiled(new File("asset/test.json")));
			} catch (ImportExportException ex) {
				throw new IllegalStateException(ex);
			}
		}
		App.network.connect("127.0.0.1", 12345); // TODO
		if (isClient()) {
			App.function.sendHandshake(App.network);
		}
	}

	private boolean isHost() {
		return App.network.getNetworkRole() == E_NetworkRole.HOST;
	}

	private boolean isClient() {
		return App.network.getNetworkRole() == E_NetworkRole.CLIENT;
	}

	@Override
	public void hide() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}
