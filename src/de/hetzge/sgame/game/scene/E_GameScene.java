package de.hetzge.sgame.game.scene;

import com.badlogic.gdx.Screen;

import de.hetzge.sgame.misc.Lazy;

/**
 * The game is splited in different scenes which are described with this enum. A
 * scene is differentiated by the input, the update loop and the rendered
 * content.
 * 
 * @author hetzge
 */
public enum E_GameScene {

	LOAD(new Lazy<>(() -> new LoadGameScene())), CONNECT(new Lazy<>(() -> new ConnectGameScene())), INGAME(new Lazy<>(() -> new IngameScene()));

	public static final E_GameScene[] values = values();

	private final Lazy<Screen> lazyScreen;

	private E_GameScene(Lazy<Screen> lazyScreen) {
		this.lazyScreen = lazyScreen;
	}

	public Screen getScreen() {
		return this.lazyScreen.get();
	}

}
